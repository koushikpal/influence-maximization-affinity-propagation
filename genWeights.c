/****************************************
	Authors: Koushik Pal
			 Zissis Poulos
****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <time.h>



int main(int argc, char *argv[])
{
	int i, u, v;					/* counters */
	double w;
	time_t t;
	char *fileNameIn = argv[1];		/* input file path passed as an argument in console */
	char *fileNameOut = argv[2];	/* output file path passed as an argument in console */

	FILE *fin = fopen(fileNameIn, "r");
	FILE *fout = fopen(fileNameOut, "w+");
	int number_of_edges;
	
	/* first row of the file is number of edges */
 	fscanf(fin, "%d", &number_of_edges);
	srand((unsigned) time(&t));

	/* parse file and construct adjacency matrix adj_matrix[][]. also extract number of vertices V and unique IDs */
	for (i = 0; i < number_of_edges; i++) {
		fscanf(fin, "%d%d", &u, &v);
		w = (double) (rand() + 1.0)/ (double) (RAND_MAX + 2.0);
		fprintf(fout, "%d %d %lf\n", u, v, w); 	
	}
	
	fclose(fin);
	fclose(fout);
	
	return 0;
}
