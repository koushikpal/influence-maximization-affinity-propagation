/****************************************
	Authors: Koushik Pal
			 Zissis Poulos
****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <time.h>

#define MAX_NODE_NUM 90000

int main(int argc, char *argv[])
{
	int i, u, v, max_nodes, k, j;					/* counters */
	double w;
	int hash_node[MAX_NODE_NUM]={0};
	char *fileNameIn = argv[1];		/* input file path passed as an argument in console */
	char *fileNameOut = argv[2];	/* output file path passed as an argument in console */

	FILE *fin = fopen(fileNameIn, "r");
	FILE *fout = fopen(fileNameOut, "w+");
	
	/* first row of the file is number of edges */
 	srand(42);
	max_nodes = 0;
	while ( fscanf(fin, "%d%d", &u, &v) ==  2)  {

		j=hash_node[u] - 1;
		k=hash_node[v] - 1;
					
		if (j < 0){
			j = max_nodes++;
			hash_node[u] = j+1;					
		}
			
		if (k < 0){
			k = max_nodes++;
			hash_node[v] = k+1;
		}
		w = (rand() + 1.0) / (RAND_MAX + 2.0);
		fprintf(fout, "%d %d %lf\n", j, k, w);
	}
	
	fclose(fin);
	fclose(fout);
	
	return 0;
}
