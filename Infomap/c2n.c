/****************************************
	Authors: Koushik Pal
			 Zissis Poulos
****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#define MAX_VERTICES 80000

typedef struct comm_t comm_t;

struct comm_t {
	int node_list[MAX_VERTICES];
	int size;
};

int main(int argc, char *argv[])
{
	
	char *fileNameIn = argv[1];
	char *fileNameOut = argv[2];							
	FILE *fin = fopen(fileNameIn, "r");
	FILE *fout = fopen(fileNameOut, "w+");

	int number_of_communities = atoi(argv[3]);
	int number_of_nodes = atoi(argv[4]);
	int comm_idx;
	int temp_index;
	int node_id;
	double flow;
	int i,j;
	
		
	comm_t *communities = (comm_t *)malloc(sizeof(comm_t) * number_of_communities);

	

	fscanf(fin, "%*[^\n]\n", NULL);
	fscanf(fin, "%*[^\n]\n", NULL);
	
	for (j=0; j<number_of_nodes; j++){
			
		fscanf(fin, "%d%d%lf", &node_id, &comm_idx, &flow);
			
		temp_index = communities[comm_idx-1].size++;
		
		communities[comm_idx-1].node_list[temp_index] = node_id;
		
		}

	for (i=0; i< number_of_communities; i++){
		fprintf(fout, "community %d with size %d: ", i+1, communities[i].size);
		for (j=0;j<communities[i].size; j++){
			fprintf(fout, "%d ", communities[i].node_list[j]);
		}
		fprintf(fout, "\n\n");
		
	}
	
	free(communities);
	fclose(fin);
	fclose(fout);
	
	return 0;
}
