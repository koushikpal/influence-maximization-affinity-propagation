/****************************************
	Authors: Koushik Pal
			 Zissis Poulos
****************************************/

#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <ctime>
#include <cmath>
#include <assert.h>
#include <math.h>
#include "exponential.h"

#define MAX_VERTICES 90000


using namespace std;


typedef struct edge_t edge_t;

struct edge_t {
	int index;											/* index of the other vertex of an edge */
	double weight;										/* edge parameter - either mean of the exponential distribution or weight or similarity on the edge */
};

vector<edge_t> graph[MAX_VERTICES];						/* the adjacency list of the graph */
bool hash_nodes[MAX_VERTICES];							/* hash map needed to count the number of (unique) vertices in the input graph */



/* comparator function for using qsort to sort exemplars by their spread */
int compare_by_spread (const void * a, const void * b)
{
	edge_t *spread_a = (edge_t *) a;
	edge_t *spread_b = (edge_t *) b;
	if ((int) spread_b->weight == (int) spread_a->weight)
		return (spread_a->index - spread_b->index);
	return (spread_b->weight - spread_a->weight);
}


/* --- priority queue functions --- */
void insert_queue(int index, double distance, double *shortest_distance, int *visited, int *heap_index, int *heap, int *heap_length)
{
	int i, j;

	/* already knew better path */
	if (visited[index] == 1 && distance >= shortest_distance[index])
		return;

	/* find existing heap entry, or create a new one */
	visited[index] = 1;
	shortest_distance[index] = distance;

	i = heap_index[index];
	if (!i) 
		i = ++(*heap_length);

	/* upheap */
	for (; i > 1 && shortest_distance[index] < shortest_distance[heap[j = i/2]]; i = j)
		heap_index[heap[i] = heap[j]] = i;

	heap[i] = index;
	heap_index[index] = i;
}


int pop_queue(double *shortest_distance, int *heap_index, int *heap, int *heap_length)
{
	int i, j;
	int index_to_return, tmp_index;

	if (*heap_length == 0)
		return -1;

	/* remove leading element, pull tail element there and downheap */
	index_to_return = heap[1];
	tmp_index = heap[(*heap_length)--];

	for (i = 1; i < *heap_length && (j = i * 2) <= *heap_length; i = j) {
		if (j < *heap_length && shortest_distance[heap[j]] > shortest_distance[heap[j+1]]) 
			j++;

		if (shortest_distance[heap[j]] >= shortest_distance[tmp_index]) 
			break;
		heap_index[heap[i] = heap[j]] = i;
	}

	heap[i] = tmp_index;
	heap_index[tmp_index] = i;

	return index_to_return;
}


/* function for computing the shortest path using priority queue dijkstra algorithm */
void dijkstra_heap(int start_index, int number_of_vertices, double deadline, double *shortest_distance, int *visited, int *heap_index, int *heap, int *heap_length)
{
	int i;												/* counter */
	int curr_index;										/* index of current vertex to process */
	int next_index;										/* index of candidate next vertex to process */
	double weight;										/* weight of current edge to process */

	insert_queue(start_index, 0.0, shortest_distance, visited, heap_index, heap, heap_length);
	while ((curr_index = pop_queue(shortest_distance, heap_index, heap, heap_length)) != -1) {
		if (shortest_distance[curr_index] >= deadline)
			break;
		for (i = 0; i < graph[curr_index].size(); i++) {
			next_index = graph[curr_index][i].index;
			weight = graph[curr_index][i].weight;
			insert_queue(next_index, shortest_distance[curr_index] + weight, shortest_distance, visited, heap_index, heap, heap_length);
		}
	}
}


/* function for computing the shortest path using dynamic dijkstra algorithm */
void dijkstra_dynamic(int start_index, int number_of_vertices, double deadline, double *shortest_distance, int *visited)
{
	int i;												/* counter */
	int index_v;										/* index of current vertex to process */
	int index_w;										/* index of candidate next vertex */
	double min_distance;								/* best current distance from start */
	double weight;										/* weight of current edge to process */

	index_v = start_index;
	shortest_distance[index_v] = 0;

	while (visited[index_v] == 0) {
		visited[index_v] = 1;
		for (i = 0; i < graph[index_v].size(); i++) {
			index_w = graph[index_v][i].index;
			weight = graph[index_v][i].weight;
			
			if (shortest_distance[index_w] > (shortest_distance[index_v] + weight))
				shortest_distance[index_w] = shortest_distance[index_v] + weight;
		}

		index_v = 0;
		min_distance = (double) INFINITY;
		for (i = 0; i < number_of_vertices; i++) {
			if ((visited[i] == 0) && (min_distance > shortest_distance[i])) {
				min_distance = shortest_distance[i];
				index_v = i;
			}
		}
		if (min_distance >= deadline)
			break;
	}
}


/* function to compute the spread of each exemplar */
void compute_spread(int number_of_vertices, int *assignments, edge_t *spread, int *count_spread)
{
	int i;
	
	bool *is_valid = new (nothrow) bool[number_of_vertices];
	if (is_valid == NULL) {
		cout << "Error: Memory could not be allocated in compute_spread." << endl;
		exit(1);
	}
	
	for (i = 0; i < number_of_vertices; i++) {
		if (assignments[i] != -1) {
			spread[assignments[i]].weight += 1;
			if (!is_valid[assignments[i]])
				count_spread[assignments[i]] += 1;
		}
	}
}


/* function for computing affinity propagation, finding exemplars and assigning nodes to exemplars */
void compute_affinity(int number_of_iterations, int number_of_vertices, double lambda, vector<edge_t> *S, int *assignments)
{
	int i, j, k, m, c, idxForI, max_index, second_max_index;
	double sum, eval, max, second_max;
	
	vector<double> *R = new (nothrow) vector<double>[number_of_vertices];
	vector<double> *A = new (nothrow) vector<double>[number_of_vertices];
	
	if (R == NULL || A == NULL) {
		cout << "Error: Memory could not be allocated in affinity." << endl;
		exit(1);
	}
	
	for (i = 0; i < number_of_vertices; i++) {
		R[i].resize(S[i].size(), 0.0);
		A[i].resize(S[i].size(), 0.0);
	}
		
	for (m = 0; m < number_of_iterations; m++) {
	
		/* update responsibility */
		for (i = 0; i < number_of_vertices; i++) {
			if (S[i].size() == 1) {
				max = S[i][0].weight + A[i][0];
				max_index = 0;
				second_max = - (double) INFINITY;
				second_max_index = -1;
			}
			else {
				if (S[i][0].weight + A[i][0] < S[i][1].weight + A[i][1]) {
					max_index = 1;
					second_max_index = 0;
				}
				else {
					max_index = 0;
					second_max_index = 1;
				}
				max = S[i][max_index].weight + A[i][max_index];
				second_max = S[i][second_max_index].weight + A[i][second_max_index];
				for (k = 2; k < S[i].size(); k++) {
					if (S[i][k].weight + A[i][k] >= max) {
						second_max = max;
						second_max_index = max_index;
						max = S[i][k].weight + A[i][k];
						max_index = k;
					}
				}
			}
			assert(max_index != second_max_index);
			
			int diff_max;
			for (j = 0; j < S[i].size(); j++) {
				if (j != max_index)
					diff_max = max;
				else
					diff_max = second_max;
				
				if (fabs(lambda) > 1E-10)
					R[i][j] = (1 - lambda) * (S[i][j].weight - diff_max) + lambda * R[i][j];
				else
					R[i][j] = S[i][j].weight - diff_max;
			}
		}
		
		/* update availability */
		double *sum_matrix = new (nothrow) double[number_of_vertices];
		if (sum_matrix == NULL) {
			cout << "Error: Memory could not be allocated inside affinity iterations." << endl;
			exit(1);
		}
		
		for (i = 0; i < number_of_vertices; i++) {
			for (j = 0; j < S[i].size(); j++) {
				k = S[i][j].index;
				if (k != i)
					sum_matrix[k] += fmax(0.0, R[i][j]);
			}
		}
		
		for (i = 0; i < number_of_vertices; i++) {
			for (j = 0; j < S[i].size(); j++) {
				k = S[i][j].index;
				if (i == k) {
					sum = sum_matrix[k];
					if (fabs(lambda) > 1E-10)
						A[i][j] = (1 - lambda) * sum + lambda * A[i][j];
					else
						A[i][j] = sum;
				} else {
					sum = sum_matrix[k] - fmax(0.0, R[i][j]);
					assert(S[k].back().index == k);
					if (fabs(lambda) > 1E-10)
						A[i][j] = (1 - lambda) * fmin(0.0, R[k].back() + sum) + lambda * A[i][j];
					else
						A[i][j] = fmin(0.0, R[k].back() + sum);
				}
			}
		}
		delete[] sum_matrix;
	}
	
	/* find the exemplars */
	bool *is_exemplar = new (nothrow) bool[number_of_vertices];
	for (i = 0; i < number_of_vertices; i++) {
		assert(S[i].back().index == i);
		eval = R[i].back() + A[i].back();
		if (eval > 0.0)
			is_exemplar[i] = true;
	}
		
	/* assign an exemplar to each node i */
	for (i = 0; i < number_of_vertices; i++) {
		idxForI = -1;
		max = -INFINITY;
		for (j = 0; j < S[i].size(); j++) {
			k = S[i][j].index;
			if (is_exemplar[k] && S[i][j].weight > max) {
				max = S[i][j].weight;
				idxForI = S[i][j].index;
			}
		}
		assignments[i] = idxForI;
		if (idxForI > -1)
			assignments[idxForI] = idxForI;
	}
	
	delete[] is_exemplar;
	delete[] R;
	delete[] A;
}


/* function to compute the similarity matrix */
void compute_similarity(int number_of_vertices, double deadline, vector<edge_t> *S)
{
	int i, j;											/* counters */

	/* variables needed for running dijkstra */
	double *shortest_distance;							/* array containing the shortest distance of each node from the source node */
	int *visited;										/* array containing 1s and 0s for nodes that are in the shortest path tree or out */
	int *heap_index;									/* index of the node in the heap */
	int *heap;											/* priority queue or min heap for implementing dijkstra */
	int heap_length;									/* number of elements in the heap */
	
	shortest_distance = new (nothrow) double[number_of_vertices];
	visited = new (nothrow) int[number_of_vertices];
	heap_index = new (nothrow) int[number_of_vertices];
	heap = new (nothrow) int[number_of_vertices + 1];
	
	deadline *= 1.15;									/* increase the deadline by 15% for running Dijkstra */
	
	/* for each node, find the shortest distance to all other nodes and estimate its total spread */
	for (i = 0; i < number_of_vertices; i++) {
		heap_length = 0;
		for (j = 0; j < number_of_vertices; j++) {
			shortest_distance[j] = (double) INFINITY;
			visited[j] = 0;
			heap_index[j] = 0;
			heap[j] = -1;
		}
		
		dijkstra_heap(i, number_of_vertices, deadline, shortest_distance, visited, heap_index, heap, &heap_length);
		//dijkstra_dynamic(i, number_of_vertices, deadline, shortest_distance, visited);
		
		for (j = 0; j < number_of_vertices; j++) {
			if (j != i && shortest_distance[j] < deadline) {
				edge_t edge;
				edge.index = j;
				edge.weight = -shortest_distance[j];
				S[i].push_back(edge);
			}
		}
	}
	
	/* assign self-similarities */
	double sum = 0.0;
	int count = 0;
	for (i = 0; i < number_of_vertices; i++) {
		for (j = 0; j < S[i].size(); j++)
			sum += S[i][j].weight;
		count += S[i].size();
	}
	if (sum >= 0.0) {
		cout << "Something is wrong with the edge weights." << endl;
		exit(1);
	}
	
	int sim_total_size = 0, sim_total_count = 0, sim_max_count = -INFINITY, sim_count_leq1 = 0, sim_count_leq5 = 0, sim_count_leq10 = 0;
	for (i = 0; i < number_of_vertices; i++) {
		edge_t edge;
		edge.index = i;
		edge.weight = sum;
		S[i].push_back(edge);
		sim_total_size += sizeof(edge_t) * S[i].size();
		sim_total_count += S[i].size();
		if (S[i].size() > sim_max_count)
			sim_max_count = S[i].size();
		if (S[i].size() == 1)
			sim_count_leq1++;
		if (S[i].size() <= 5)
			sim_count_leq5++;
		if (S[i].size() <= 10)
			sim_count_leq10++;
	}
	deadline /= 1.15;
	cout << "Deadline = " << deadline << endl;
	cout << "Total size of the similarity adjacency list (in MB) = " << (double) sim_total_size / 1000000.0 << endl;
	cout << "Total number of entries in the similarity adjacency list = " << sim_total_count << endl;
	cout << "Maximum length of any row in the similarity adjacency list = " << sim_max_count << endl;
	cout << "Number of rows of the similarity adjacency list of size less than or equal to 1 = " << sim_count_leq1 << endl;
	cout << "Number of rows of the similarity adjacency list of size less than or equal to 5 = " << sim_count_leq5 << endl;
	cout << "Number of rows of the similarity adjacency list of size less than or equal to 10 = " << sim_count_leq10 << endl;
	
	/* free allocated memory */
	delete[] shortest_distance;
	delete[] visited;
	delete[] heap_index;
	delete[] heap;
}


/* function to read a graph from a file */
void read_graph(char *fileName_graph, int *number_of_vertices)
{
	int i, u, v, number_of_edges = 0;
	double alpha, beta;
	ifstream infile;
	
	infile.open(fileName_graph, ios::in);
	if (!infile.is_open()) {
		cout << "Error: File not found." << endl;
		exit(1);
	}

	/* parse file and construct adjacency matrix. also extract number of vertices and number of edges. */
	while (infile >> u >> v >> alpha >> beta) {
		if (hash_nodes[u] == 0) {
			(*number_of_vertices)++;
			hash_nodes[u] = 1;
		}
		if (hash_nodes[v] == 0) {
			(*number_of_vertices)++;
			hash_nodes[v] = 1;
		}
		
		edge_t edge;
		edge.index = u;
		edge.weight = alpha * beta;
		assert(v < MAX_VERTICES);
		graph[v].push_back(edge);
		number_of_edges++;
	}
	
	int node_total_size = 0, node_total_count = 0, node_max_count = 0, node_count_leq1 = 0, node_count_leq5 = 0, node_count_leq10 = 0;
	for (i = 0; i < *number_of_vertices; i++) {
		node_total_size += sizeof(edge_t) * graph[i].size();
		node_total_count += graph[i].size();
		if (graph[i].size() > node_max_count)
			node_max_count = graph[i].size();
		if (graph[i].size() <= 1)
			node_count_leq1++;
		if (graph[i].size() <= 5)
			node_count_leq5++;
		if (graph[i].size() <= 10)
			node_count_leq10++;
	}
	if (node_total_count != number_of_edges)
		cout << "Warning: Node total count = " << node_total_count << ", number of edges = " << number_of_edges << endl;
	cout << "Total number of vertices = " << *number_of_vertices << endl;
	cout << "Total size of the graph adjacency list (in MB) = " << (double) node_total_size / 1000000.0 << endl;
	cout << "Total number of entries in the graph adjacency list = " << node_total_count << endl;
	cout << "Maximum length of any row in the graph adjacency list = " << node_max_count << endl;
	cout << "Number of rows of the graph adjacency list of size less than or equal to 1 = " << node_count_leq1 << endl;
	cout << "Number of rows of the graph adjacency list of size less than or equal to 5 = " << node_count_leq5 << endl;
	cout << "Number of rows of the graph adjacency list of size less than or equal to 10 = " << node_count_leq10 << endl;
	
	infile.close();
}


/* function to print the adjacency list of the graph */
void print_graph(int number_of_vertices)
{
	int i, j;
	cout << "The following is the adjacency list of the graph:" << endl;
	for (i = 0; i < number_of_vertices; i++) {
		cout << setw(10) << i;
		for(j = 0; j < graph[i].size(); j++)
			cout << setw(10) << graph[i][j].index;
		cout << endl;		
	}
	cout << endl;
}


/* function to print the adjacency matrix of weights */
void print_weights(int number_of_vertices)
{
	int i, j;
	cout << "The following is the adjacency list of the parameters of the edge distributions:" << endl;
	for (i = 0; i < number_of_vertices; i++) {
		cout << setw(10) << i;
		for(j = 0; j < graph[i].size(); j++)
			cout << setw(10) << graph[i][j].weight;
		cout << endl;		
	}
	cout << endl;
}


/* function to print the similarity matrix */
void print_similarity(int number_of_vertices, vector<edge_t> *S)
{
	int i, j;
	cout << "The following is the adjacency list of the similarities:" << endl;
	for (i = 0; i < number_of_vertices; i++) {
		if (S[i].size() > 0) {
			cout << setw(10) << i;
			for (j = 0; j < S[i].size(); j++)
				cout << setw(10) << S[i][j].weight;
			cout << endl;
		}
	}
	cout << endl;
}


/* function to print the exemplars */
void print_exemplars(int number_of_vertices, int* assignments)
{
	int i;
	cout << "The following nodes are the exemplars: ";
	for (i = 0; i < number_of_vertices; i++) {
		if (assignments[i] == i)
			cout << setw(10) << i;
	}
	cout << endl;
}


/* function to print the assignments */
void print_assignments(int number_of_vertices, int *assignments)
{
	int i, count = 0;
	cout << "The following are the assignments of nodes to exemplars:" << endl;
	for (i = 0; i < number_of_vertices; i++)
		cout << setw(10) << i << " -> " << setw(10) << assignments[i] << endl;
}


/* function to print the total spread of each exemplar */
void print_spread(int number_of_vertices, edge_t *spread)
{
	int i;
	cout << "The following is the spread of each exemplar:" << endl;
	for (i = 0; i < number_of_vertices; i++) {
		if (spread[i].weight > 0)
			cout << "Spread of node " << setw(10) << spread[i].index << " = " << setw(10) << spread[i].weight << endl;
	}
	cout << endl;
}


void print_count_spread(int number_of_vertices, int *count_spread)
{
	int i;
	cout << "The following is the count spread of each exemplar:" << endl;
	for (i = 0; i < number_of_vertices; i++) {
		if (count_spread[i] > 0)
			cout << "Spread of node " << setw(10) << i << " = " << setw(10) << count_spread[i] << endl;
	}
	cout << endl;
}



int main(int argc, char *argv[])
{
	int i, j, k;										/* counters */
	double time = 0;									/* time counter */
	int number_of_vertices = 0;							/* graph parameters */
	int number_of_iterations;							/* number of iterations the program runs */
	int number_of_samples;								/* number of times to sample from the path distributions */
	double lambda = 0.8;								/* damping factor */
	double deadline = INFINITY;								/* deadline */
	clock_t start, stop;								/* variables for measuring time taken by the CPU */
	
	char *fileName = argv[1];							/* input file path to read the graph from passed as an argument in console */
	number_of_iterations = atoi(argv[2]);				/* number of iterations passed as argument in console */
	number_of_samples = atoi(argv[3]);					/* number of samples passed as argument in console */

	cout << "Reading graph from file" << endl;
	start = clock();
	read_graph(fileName, &number_of_vertices);
	stop = clock();
	cout << "Done reading graph from file in " << (double) (stop - start)/CLOCKS_PER_SEC << " seconds\n" << endl;
	time += (double) (stop - start)/CLOCKS_PER_SEC;
	
	//print_graph(number_of_vertices);
	//print_weights(number_of_vertices);
	
	cout << "Computing similarity adjacency list" << endl;
	start = clock();
	vector<edge_t> *S = new (nothrow) vector<edge_t>[number_of_vertices];
	if (S == NULL) {
		cout << "Error: Memory could not be allocated." << endl;
		exit(1);
	}
	compute_similarity(number_of_vertices, deadline, S);
	stop = clock();
	cout << "Done computing similarity adjacency list in " << (double) (stop - start)/CLOCKS_PER_SEC << " seconds\n" << endl;
	time += (double) (stop - start)/CLOCKS_PER_SEC;
	double time1 = time;
	
	//print_similarity(number_of_vertices, S);
	
	cout << "Computing affinity scores, selecting exemplars and computing spread" << endl;
	rand_val(start);									/* set the seed */
	edge_t *spread = new (nothrow) edge_t[number_of_vertices];
	int *count_spread = new (nothrow) int[number_of_vertices];
	if (spread == NULL || count_spread == NULL) {
		cout << "Error: Memory could not be allocated." << endl;
		exit(1);
	}
	for (i = 0; i < number_of_vertices; i++) {
		spread[i].index = i;
		spread[i].weight = 0;
		count_spread[i] = 0;
	}
	for (k = 0; k < number_of_samples; k++) {
		start = clock();
		cout << "Sample: " << k + 1 << endl;
		vector<edge_t> *T = new (nothrow) vector<edge_t>[number_of_vertices];
		if (T == NULL) {
			cout << "Error: Memory could not be allocated." << endl;
			exit(1);
		}
		for (i = 0; i < number_of_vertices; i++) {
			for (j = 0; j < S[i].size(); j++) {
				edge_t edge;
				edge.index = S[i][j].index;
				double weight;
				do {
					weight = exponential(-S[i][j].weight);
				} while (weight >= deadline);
				if (weight < deadline)
					count_spread[i] += 1;
				edge.weight = -weight;
				T[i].push_back(edge);
			}
		}
		
		int *assignments = new (nothrow) int[number_of_vertices];
		if (assignments == NULL) {
			cout << "Error: Memory could not be allocated." << endl;
			exit(1);
		}
		compute_affinity(number_of_iterations, number_of_vertices, lambda, T, assignments);
		compute_spread(number_of_vertices, assignments, spread, count_spread);
		
		stop = clock();
		cout << "Done computing sample " << k + 1 << " in " << (double) (stop - start)/CLOCKS_PER_SEC << " seconds" << endl;
		time += (double) (stop - start)/CLOCKS_PER_SEC;
		delete[] T;
		delete[] assignments;
	}
	/*for (i = 0; i < number_of_vertices; i++) {
		assert(spread[i].index == i);
		spread[i].weight *= (double) count_spread[i] / (double) number_of_samples;
	}*/
	qsort(spread, number_of_vertices, sizeof(edge_t), compare_by_spread);
	cout << "Done computing affinity scores, selecting exemplars and computing spread in " << (time - time1) << " seconds\n" << endl;
	
	cout << "Final Results:" << endl;
	print_spread(number_of_vertices, spread);
	cout << "Total time taken = " << time << " seconds." << endl;
	
	/* free allocated memory */
	delete[] spread;
	delete[] count_spread;
	delete[] S;
	
	return 0;
}