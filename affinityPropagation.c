/****************************************
	Authors: Koushik Pal
			 Zissis Poulos
****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <time.h>


#define MAX_VERTICES 80000
#define MAX_OUTDEGREE 5000


typedef struct node_t node_t;

struct node_t {
	int ID;												/* unique ID of the node */
	int index;											/* location of the node in the global nodes array */
	int in_degree;										/* in-degree of the node in the graph */
	int out_degree;										/* out-degree of the node in the graph */
	int outgoing_edges[MAX_OUTDEGREE];					/* list of edges going out from the node */
};

node_t nodes[MAX_VERTICES] = {0};						/* the adjacency list of the graph */
int hash_nodes[MAX_VERTICES] = {0};						/* the ID -> index mapping, stores index + 1 */
double weights[MAX_VERTICES * MAX_OUTDEGREE] = {0};		/* the weights on the edges of the graph */


/* --- priority queue functions --- */
void insert_queue(int index, double distance, double *shortest_distance, int *visited, int *heap_index, int *heap, int *heap_length)
{
	int i, j;

	/* already knew better path */
	if (visited[index] == 1 && distance >= shortest_distance[index])
		return;

	/* find existing heap entry, or create a new one */
	visited[index] = 1;
	shortest_distance[index] = distance;

	i = heap_index[index];
	if (!i) 
		i = ++(*heap_length);

	/* upheap */
	for (; i > 1 && shortest_distance[index] < shortest_distance[heap[j = i/2]]; i = j)
		heap_index[heap[i] = heap[j]] = i;

	heap[i] = index;
	heap_index[index] = i;
}


int pop_queue(double *shortest_distance, int *heap_index, int *heap, int *heap_length)
{
	int i, j;
	int index_to_return, tmp_index;

	if (*heap_length == 0)
		return -1;

	/* remove leading element, pull tail element there and downheap */
	index_to_return = heap[1];
	tmp_index = heap[(*heap_length)--];

	for (i = 1; i < *heap_length && (j = i * 2) <= *heap_length; i = j) {
		if (j < *heap_length && shortest_distance[heap[j]] > shortest_distance[heap[j+1]]) 
			j++;

		if (shortest_distance[heap[j]] >= shortest_distance[tmp_index]) 
			break;
		heap_index[heap[i] = heap[j]] = i;
	}

	heap[i] = tmp_index;
	heap_index[tmp_index] = i;

	return index_to_return;
}


/* function for computing the shortest path using priority queue dijkstra algorithm */
void dijkstra_heap(int start_index, int number_of_vertices, double *shortest_distance, int *visited, int *heap_index, int *heap, int *heap_length)
{
	int i;										/* counter */
	int curr_index;								/* index of current vertex to process */
	int next_index;								/* index of candidate next vertex to process */

	insert_queue(start_index, 0.0, shortest_distance, visited, heap_index, heap, heap_length);
	while ((curr_index = pop_queue(shortest_distance, heap_index, heap, heap_length)) != -1) {
		for (i = 0; i < nodes[curr_index].out_degree; i++) {
			next_index = nodes[curr_index].outgoing_edges[i];
			insert_queue(next_index, shortest_distance[curr_index] + weights[curr_index * MAX_OUTDEGREE + next_index], shortest_distance, visited, heap_index, heap, heap_length);
		}
	}
}


/* function for computing the shortest path using dynamic dijkstra algorithm */
void dijkstra_dynamic(int start_index, int number_of_vertices, double *shortest_distance, int *visited)
{
	int i;										/* counter */
	int index_v;								/* index of current vertex to process */
	int index_w;								/* index of candidate next vertex */
	double min_distance;						/* best current distance from start */
	double weight;								/* weight of current edge to process */

	index_v = start_index;
	shortest_distance[index_v] = 0;

	while (visited[index_v] == 0) {
		visited[index_v] = 1;
		for (i = 0; i < nodes[index_v].out_degree; i++) {
			index_w = nodes[index_v].outgoing_edges[i];
			weight = weights[index_v * MAX_OUTDEGREE + index_w];
			
			if (shortest_distance[index_w] > (shortest_distance[index_v] + weight))
				shortest_distance[index_w] = shortest_distance[index_v] + weight;
		}

		index_v = 0;
		min_distance = (double) INFINITY;
		for (i = 0; i < number_of_vertices; i++) {
			if ((visited[i] == 0) && (min_distance > shortest_distance[i])) {
				min_distance = shortest_distance[i];
				index_v = i;
			}
		}
	}
}


/* function to compute the spread of each exemplar */
void compute_spread(int number_of_vertices, int *assignments, int *spread)
{
	int i;
	
	for (i = 0; i < number_of_vertices; i++)
		spread[i] = 0;
	for (i = 0; i < number_of_vertices; i++)
		spread[assignments[i]]++;
}


/* function for computing affinity propagation, finding exemplars and assigning nodes to exemplars */
void compute_affinity(int number_of_iterations, int number_of_vertices, double lambda, double *S, int *exemplars, int *assignments, int *number_of_exemplars)
{
	int i, j, k, m, c, idxForI, max_index, second_max_index;
	double sum, eval, max, second_max;
	
	double *R = (double *)malloc(sizeof(double) * number_of_vertices * number_of_vertices);
	double *A = (double *)malloc(sizeof(double) * number_of_vertices * number_of_vertices);
	double *sum_matrix = (double *)malloc(sizeof(double) * number_of_vertices);
		
	for (m = 0; m < number_of_iterations; m++) {
		printf("In iteration %d:\n", m);
	
		/* update responsibility */
		for (i = 0; i < number_of_vertices; i++) {
			if (S[i * number_of_vertices + 0] + A[i * number_of_vertices + 0] < S[i * number_of_vertices + 1] + A[i * number_of_vertices + 1]) {
				max = S[i * number_of_vertices + 1] + A[i * number_of_vertices + 1];
				max_index = 1;
				second_max = S[i * number_of_vertices + 0] + A[i * number_of_vertices + 0];
				second_max_index = 0;
			}
			else {
				max = S[i * number_of_vertices + 0] + A[i * number_of_vertices + 0];
				max_index = 0;
				second_max = S[i * number_of_vertices + 1] + A[i * number_of_vertices + 1];
				second_max_index = 1;
			}
			for (k = 2; k < number_of_vertices; k++) {
				if (S[i * number_of_vertices + k] + A[i * number_of_vertices + k] >= max) {
					second_max = max;
					second_max_index = max_index;
					max = S[i * number_of_vertices + k] + A[i * number_of_vertices + k];
					max_index = k;
				}
				else if (S[i * number_of_vertices + k] + A[i * number_of_vertices + k] >= second_max) {
					second_max = S[i * number_of_vertices + k] + A[i * number_of_vertices + k];
					second_max_index = k;
				}
			}
			printf("Max = %lf, Second max = %lf, Max index = %d, Second max index = %d\n", max, second_max, max_index, second_max_index);
			assert(max_index != second_max_index);
			
			for (j = 0; j < number_of_vertices; j++) {
				if (j != max_index) {
					if (fabs(lambda) > 1E-10)
						R[i * number_of_vertices + j] = (1 - lambda) * (S[i * number_of_vertices + j] - max) + lambda * R[i * number_of_vertices + j];
					else
						R[i * number_of_vertices + j] = S[i * number_of_vertices + j] - max;
					printf("R[%d][%d] = %lf\n", i, j, R[i * number_of_vertices + j]);
				}
				else {
					if (fabs(lambda) > 1E-10)
						R[i * number_of_vertices + j] = (1 - lambda) * (S[i * number_of_vertices + j] - second_max) + lambda * R[i * number_of_vertices + j];
					else
						R[i * number_of_vertices + j] = S[i * number_of_vertices + j] - second_max;
					printf("R[%d][%d] = %lf\n", i, j, R[i * number_of_vertices + j]);
				}
			}
		}
		
		/* update availability */
		for (j = 0; j < number_of_vertices; j++) {
			sum_matrix[j] = 0.0;
			for (k = 0; k < number_of_vertices; k++) {
				if (k != j)
					sum_matrix[j] += fmax(0.0, R[k * number_of_vertices + j]);
			}
			printf("sum_matrix[%d] = %lf\n", j, sum_matrix[j]);
		}
		
		for (i = 0; i < number_of_vertices; i++) {
			for (j = 0; j < number_of_vertices; j++) {
				if (i == j) {
					sum = sum_matrix[j];
					if (fabs(lambda) > 1E-10)
						A[i * number_of_vertices + j] = (1 - lambda) * sum + lambda * A[i * number_of_vertices + j];
					else
						A[i * number_of_vertices + j] = sum;
					printf("A[%d][%d] = %lf\n", i, j, A[i * number_of_vertices + j]);
				} else {
					sum = sum_matrix[j] - fmax(0.0, R[i * number_of_vertices + j]);
					if (fabs(lambda) > 1E-10)
						A[i * number_of_vertices + j] = (1 - lambda) * fmin(0.0, R[j * number_of_vertices + j] + sum) + lambda * A[i * number_of_vertices + j];
					else
						A[i * number_of_vertices + j] = fmin(0.0, R[j * number_of_vertices + j] + sum);
					printf("A[%d][%d] = %lf\n", i, j, A[i * number_of_vertices + j]);
				}
			}
		}
		
	}
	
	/* find the exemplars */
	for (i = 0; i < number_of_vertices; i++) {
		eval = R[i * number_of_vertices + i] + A[i * number_of_vertices + i];
		printf("Eval[%d] = %lf\n", i, eval);
		if (eval > 0.0)
			exemplars[(*number_of_exemplars)++] = i;
	}
	
	/* assign an exemplar to each node i */
	for (i = 0; i < number_of_vertices; i++) {
		idxForI = 0;
		max = -INFINITY;
		for (j = 0; j < *number_of_exemplars; j++) {
			c = exemplars[j];
			if (S[i * number_of_vertices + c] > max) {
				max = S[i * number_of_vertices + c];
				idxForI = c;
			}
		}
		assignments[i] = idxForI;
	}
	
	free(R);
	free(A);
	free(sum_matrix);
}


/* function to compute the similarity matrix */
void compute_similarity(int number_of_vertices, int number_of_edges, double deadline, double *S)
{
	int i, j;											/* counters */

	/* variables needed for running dijkstra */
	double *shortest_distance;							/* array containing the shortest distance of each node from the source node */
	int *visited;										/* array containing 1s and 0s for nodes that are in the shortest path tree or out */
	int *heap_index;									/* index of the node in the heap */
	int *heap;											/* priority queue or min heap for implementing dijkstra */
	int heap_length;									/* number of elements in the heap */
	
	shortest_distance = (double *)malloc(sizeof(double) * number_of_vertices);
	visited = (int *)malloc(sizeof(int) * number_of_vertices);
	heap_index = (int *)malloc(sizeof(int) * number_of_vertices);
	heap = (int *)malloc(sizeof(int) * (number_of_vertices + 1));
	
	/* for each node, find the shortest distance to all other nodes and estimate its total spread */
	for (j = 0; j < number_of_vertices; j++) {
		heap_length = 0;
		for (i = 0; i < number_of_vertices; i++) {
			shortest_distance[i] = (double) INFINITY;
			visited[i] = 0;
			heap_index[i] = 0;
			heap[i] = -1;
		}
		
		dijkstra_heap(j, number_of_vertices, shortest_distance, visited, heap_index, heap, &heap_length);
		//dijkstra_dynamic(j, number_of_vertices, shortest_distance, visited);
		
		for (i = 0; i < number_of_vertices; i++) {
			if (shortest_distance[i] < deadline)
				S[j * number_of_vertices + i] = -shortest_distance[i];
			else
				S[j * number_of_vertices + i] = -INFINITY;
		}
	}
	
	/* assign self-similarities */
	double sum = 0.0;
	int count = 0;
	for (i = 0; i < number_of_vertices; i++) {
		for (j = 0; j < number_of_vertices; j++) {
			if (S[i * number_of_vertices + j] > -INFINITY) {
				sum += S[i * number_of_vertices + j];
				count++;
			}
		}
	}
	
	if (sum >= 0.0)
		sum = -INFINITY;
	
	for (i = 0; i < number_of_vertices; i++)
		S[i * number_of_vertices + i] = sum;
	
	/* free allocated memory */
	free(shortest_distance);
	free(visited);
	free(heap_index);
	free(heap);
}


/* function to read a graph from a file */
void read_graph(char *fileName_graph, int *number_of_vertices, int *number_of_edges)
{
	int num_vertices = 0;
	int j, k, u, v;
	double w;
	
	FILE *fin = fopen(fileName_graph, "r");

	/* parse file and construct adjacency matrix. also extract number of vertices, number of edges and unique IDs */
	while (fscanf(fin, "%d%d%lf", &u, &v, &w) == 3) {
		
		j = hash_nodes[u] - 1;
		if (j >= 0)
			nodes[j].in_degree++;
		else {
			nodes[num_vertices].ID = u;
			nodes[num_vertices].index = num_vertices;
			nodes[num_vertices].in_degree = 1;
			nodes[num_vertices].out_degree = 0;
			j = num_vertices;
			hash_nodes[u] = ++num_vertices;
		}
		
		k = hash_nodes[v] - 1;
		if (k >= 0) {
			nodes[k].out_degree++;
			assert(nodes[k].out_degree < MAX_OUTDEGREE);
		}
		else {
			nodes[num_vertices].ID = v;
			nodes[num_vertices].index = num_vertices;
			nodes[num_vertices].in_degree = 0;
			nodes[num_vertices].out_degree = 1;
			k = num_vertices;
			hash_nodes[v] = ++num_vertices;
		}
		
		nodes[k].outgoing_edges[nodes[k].out_degree - 1] = j;
		(*number_of_edges)++;
		weights[k * MAX_OUTDEGREE + j] = -log(w);
	}
	
	fclose(fin);
	
	*number_of_vertices = num_vertices;
	assert(num_vertices < MAX_VERTICES);
}


/* function to print the adjacency list of the graph */
void print_graph(int number_of_vertices)
{
	int i, j;
	printf("The following is the adjacency list of the graph:\n");
	for (i = 0; i < number_of_vertices; i++) {
		printf("%10d ", nodes[i].ID);
		for(j = 0; j < nodes[i].out_degree; j++)
			printf("%10d ", nodes[nodes[i].outgoing_edges[j]].ID);
		printf("\n");		
	}
	printf("\n");
}


/* function to print the adjacency matrix of weights */
void print_weights(int number_of_vertices)
{
	int i, j;
	printf("The following is the adjacency matrix of the weights:\n");
	for (i = 0; i < number_of_vertices; i++) {
		for(j = 0; j < number_of_vertices; j++)
			printf("%10lf ", weights[i * MAX_OUTDEGREE + j]);
		printf("\n");		
	}
	printf("\n");
}


/* function to print the similarity matrix */
void print_similarity(int number_of_vertices, double *S)
{
	int i, j;
	printf("The following is the similarity matrix based on shortest paths:\n");
	for (i = 0; i < number_of_vertices; i++) {
		for (j = 0; j < number_of_vertices; j++)
			printf("%10lf ", S[i * number_of_vertices + j]);
		printf("\n");
	}
	printf("\n");
}


/* function to print the exemplars */
void print_exemplars(int number_of_exemplars, int *exemplars)
{
	int i;
	printf("The number of exemplars obtained = %d\n", number_of_exemplars);
	printf("The following nodes are the exemplars:");
	for (i = 0; i < number_of_exemplars; i++)
		printf("%10d ", nodes[exemplars[i]].ID);
	printf("\n");
}


/* function to print the assignments */
void print_assignments(int number_of_vertices, int *assignments)
{
	int i;
	printf("The following are the assignments of nodes to exemplars:\n");
	for (i = 0; i < number_of_vertices; i++)
		printf("%10d -> %10d\n", nodes[i].ID, nodes[assignments[i]].ID);
}


/* function to print the total spread of each exemplar */
void print_spread(int number_of_vertices, int *spread)
{
	int i;
	printf("The following is the spread of each exemplar\n");
	for (i = 0; i < number_of_vertices; i++) {
		if (spread[i] > 1)
			printf("Spread of Node %10d = %10d\n", nodes[i].ID, spread[i]);
	}
	printf("\n");
}


int main(int argc, char *argv[])
{
	int i, j;											/* counters */
	double time = 0;									/* time counter */
	int number_of_vertices, number_of_edges;			/* graph parameters */
	int number_of_iterations;							/* number of iterations the program runs */
	double lambda = 0.8;								/* damping factor */
	double deadline = (double) INFINITY;				/* deadline */
	clock_t start, stop;								/* variables for measuring time taken by the CPU */
	
	char *fileName = argv[1];							/* input file path to read the graph from passed as an argument in console */
	number_of_iterations = atoi(argv[2]);				/* no. of iterations passed as argument in console */

	printf("Reading graph from file\n");
	start = clock();
	read_graph(fileName, &number_of_vertices, &number_of_edges);
	stop = clock();
	printf("Done reading graph from file in %lf seconds\n", (double) (stop - start)/CLOCKS_PER_SEC);
	time += (double) (stop - start)/CLOCKS_PER_SEC;
	printf("Number of vertices = %d\n", number_of_vertices);
	printf("Number of edges = %d\n", number_of_edges);
	
	//print_graph(number_of_vertices);
	//print_weights(number_of_vertices);
	
	int number_of_exemplars = 0;
	int *exemplars = (int *)malloc(sizeof(int) * number_of_vertices);
	int *assignments = (int *)malloc(sizeof(int) * number_of_vertices);
	int *spread = (int *)malloc(sizeof(int) * number_of_vertices);
	double *S = (double *)malloc(sizeof(double) * number_of_vertices * number_of_vertices);

	printf("Computing similarity matrix\n");
	start = clock();
	compute_similarity(number_of_vertices, number_of_edges, deadline, S);
	stop = clock();
	printf("Done computing similarity matrix in %lf seconds\n", (double) (stop - start)/CLOCKS_PER_SEC);
	time += (double) (stop - start)/CLOCKS_PER_SEC;
	//print_similarity(number_of_vertices, S);
	
	printf("Computing affinity scores and selecting exemplars\n");
	start = clock();
	compute_affinity(number_of_iterations, number_of_vertices, lambda, S, exemplars, assignments, &number_of_exemplars);
	stop = clock();
	printf("Done computing affinity scores and selecting exemplars in %lf seconds\n", (double) (stop - start)/CLOCKS_PER_SEC);
	time += (double) (stop - start)/CLOCKS_PER_SEC;
	
	printf("Computing the spread of each exemplar and sorting them by spread\n");
	start = clock();
	compute_spread(number_of_vertices, assignments, spread);
	stop = clock();
	printf("Done computing the spread of each exemplar and sorting them by spread in %lf seconds\n", (double) (stop - start)/CLOCKS_PER_SEC);
	time += (double) (stop - start)/CLOCKS_PER_SEC;
	
	printf("\nFinal Results:\n");
	print_exemplars(number_of_exemplars, exemplars);
	//print_assignments(number_of_vertices, assignments);
	print_spread(number_of_vertices, spread);
	printf("Total time taken = %lf seconds.\n", time);
	
	/* free allocated memory */
	free(exemplars);
	free(assignments);
	free(spread);
	free(S);
	
	return 0;
}
