/****************************************
	Authors: Koushik Pal
			 Zissis Poulos
****************************************/

#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <ctime>
#include <cmath>
#include <assert.h>
#include <math.h>
#include "exponential.h"

#define MAX_VERTICES 90000


using namespace std;


typedef struct edge_t edge_t;

struct edge_t {
	int index;											/* index of the other vertex of an edge */
	double weight;										/* edge parameter - either mean of the exponential distribution or weight or similarity on the edge */
};

vector<edge_t> graph[MAX_VERTICES];						/* the adjacency list of the graph */
bool hash_nodes[MAX_VERTICES];							/* hash map needed to count the number of (unique) vertices in the input graph */



/* --- priority queue functions --- */
void insert_queue(int index, double distance, double *shortest_distance, int *visited, int *heap_index, int *heap, int *heap_length)
{
	int i, j;

	/* already knew better path */
	if (visited[index] == 1 && distance >= shortest_distance[index])
		return;

	/* find existing heap entry, or create a new one */
	visited[index] = 1;
	shortest_distance[index] = distance;

	i = heap_index[index];
	if (!i) 
		i = ++(*heap_length);

	/* upheap */
	for (; i > 1 && shortest_distance[index] < shortest_distance[heap[j = i / 2]]; i = j)
		heap_index[heap[i] = heap[j]] = i;

	heap[i] = index;
	heap_index[index] = i;
}


int pop_queue(double *shortest_distance, int *heap_index, int *heap, int *heap_length)
{
	int i, j;
	int index_to_return, tmp_index;

	if (*heap_length == 0)
		return -1;

	/* remove leading element, pull tail element there and downheap */
	index_to_return = heap[1];
	tmp_index = heap[(*heap_length)--];

	for (i = 1; i < *heap_length && (j = i * 2) <= *heap_length; i = j) {
		if (j < *heap_length && shortest_distance[heap[j]] > shortest_distance[heap[j + 1]]) 
			j++;

		if (shortest_distance[heap[j]] >= shortest_distance[tmp_index]) 
			break;
		heap_index[heap[i] = heap[j]] = i;
	}

	heap[i] = tmp_index;
	heap_index[tmp_index] = i;

	return index_to_return;
}


/* function for computing the shortest path using priority queue dijkstra algorithm */
void dijkstra_heap(int start_index, int number_of_vertices, double deadline, double *shortest_distance, int *visited, int *heap_index, int *heap, int *heap_length)
{
	int i;												/* counter */
	int curr_index;										/* index of current vertex to process */
	int next_index;										/* index of candidate next vertex to process */
	double weight;										/* weight of current edge to process */
	
	insert_queue(start_index, 0.0, shortest_distance, visited, heap_index, heap, heap_length);
	while ((curr_index = pop_queue(shortest_distance, heap_index, heap, heap_length)) != -1) {
		if (shortest_distance[curr_index] >= deadline)
			break;
		for (i = 0; i < graph[curr_index].size(); i++) {
			next_index = graph[curr_index][i].index;
			weight = graph[curr_index][i].weight;
			insert_queue(next_index, shortest_distance[curr_index] + weight, shortest_distance, visited, heap_index, heap, heap_length);
	}	}
}


/* function to compute the extended adjacency list of the graph */
void compute_extended_adjacency(int number_of_vertices, double deadline, vector<edge_t> *S)
{
	int i, j;											/* counters */

	/* variables needed for running dijkstra */
	double *shortest_distance;							/* array containing the shortest distance of each node from the source node */
	int *visited;										/* array containing 1s and 0s for nodes that are in the shortest path tree or out */
	int *heap_index;									/* index of the node in the heap */
	int *heap;											/* priority queue or min heap for implementing dijkstra */
	int heap_length;									/* number of elements in the heap */
	
	shortest_distance = new (nothrow) double[number_of_vertices];
	visited = new (nothrow) int[number_of_vertices];
	heap_index = new (nothrow) int[number_of_vertices];
	heap = new (nothrow) int[number_of_vertices + 1];
	
	/* for each node, find the shortest distance to all other nodes and estimate its total spread */
	for (i = 0; i < number_of_vertices; i++) {
		heap_length = 0;
		for (j = 0; j < number_of_vertices; j++) {
			shortest_distance[j] = (double) INFINITY;
			visited[j] = 0;
			heap_index[j] = 0;
			heap[j] = -1;
		}
	
		dijkstra_heap(i, number_of_vertices, deadline, shortest_distance, visited, heap_index, heap, &heap_length);
	
		for (j = 0; j < number_of_vertices; j++) {
			if (shortest_distance[j] < deadline) {
				edge_t edge;
				edge.index = j;
				edge.weight = shortest_distance[j];
				S[i].push_back(edge);
	}	}	}
	
	long sim_total_size = 0.0, sim_total_count = 0.0, sim_max_count = -INFINITY;
	for (i = 0; i < number_of_vertices; i++) {
		sim_total_size += sizeof(edge_t) * S[i].size();
		sim_total_count += S[i].size();
		if (S[i].size() > sim_max_count)
			sim_max_count = S[i].size();
	}
	
	cout << "Total size of the extended adjacency list (in MB) = " << (double) sim_total_size / 1000000.0 << endl;
	cout << "Total number of entries in the extended adjacency list = " << sim_total_count << endl;
	cout << "Maximum length of any row in the extended adjacency list = " << sim_max_count << endl;
	
	/* free allocated memory */
	delete[] shortest_distance;
	delete[] visited;
	delete[] heap_index;
	delete[] heap;
}


/* function to read a graph from a file */
void read_graph(char *fileName_graph, int *number_of_vertices)
{
	int i, u, v, number_of_edges = 0;
	double alpha, beta;
	ifstream infile;
	
	infile.open(fileName_graph, ios::in);
	if (!infile.is_open()) {
		cout << "Error: File not found." << endl;
		exit(1);
	}

	/* parse file and construct adjacency matrix. also extract number of vertices and number of edges. */
	while (infile >> u >> v >> alpha >> beta) {
		if (hash_nodes[u] == 0) {
			(*number_of_vertices)++;
			hash_nodes[u] = 1;
		}
		if (hash_nodes[v] == 0) {
			(*number_of_vertices)++;
			hash_nodes[v] = 1;
		}
		
		edge_t edge;
		edge.index = v;
		edge.weight = alpha * beta;			// we assume the edge distributions are Gamma(alpha, beta), where alpha is the shape and beta is the scale parameter.
		assert(u < MAX_VERTICES);
		graph[u].push_back(edge);
		number_of_edges++;
	}
	
	long node_total_size = 0, node_total_count = 0, node_max_count = 0;
	for (i = 0; i < *number_of_vertices; i++) {
		node_total_size += sizeof(edge_t) * graph[i].size();
		node_total_count += graph[i].size();
		if (graph[i].size() > node_max_count)
			node_max_count = graph[i].size();
	}
	if (node_total_count != number_of_edges)
		cout << "Warning: Node total count = " << node_total_count << ", number of edges = " << number_of_edges << endl;
	cout << "Total number of vertices = " << *number_of_vertices << endl;
	cout << "Total size of the graph adjacency list (in MB) = " << (double) node_total_size / 1000000.0 << endl;
	cout << "Total number of entries in the graph adjacency list = " << node_total_count << endl;
	cout << "Maximum length of any row in the graph adjacency list = " << node_max_count << endl;
	
	infile.close();
}


/* function to print the adjacency list of the graph */
void print_graph(int number_of_vertices)
{
	int i, j;
	cout << "The following is the adjacency list of the graph:" << endl;
	for (i = 0; i < number_of_vertices; i++) {
		cout << setw(10) << i;
		for(j = 0; j < graph[i].size(); j++)
			cout << setw(10) << graph[i][j].index << " (" << graph[i][j].weight << ") ";
		cout << endl;		
	}
	cout << endl;
}


/* function to print the similarity matrix */
void print_similarity(int number_of_vertices, vector<edge_t> *S)
{
	int i, j;
	cout << "The following is the extended adjacency list of the graph:" << endl;
	for (i = 0; i < number_of_vertices; i++) {
		if (S[i].size() > 0) {
			cout << setw(10) << i;
			for (j = 0; j < S[i].size(); j++)
				cout << setw(10) << S[i][j].index << " (" << S[i][j].weight << ") ";
			cout << endl;
	}	}
	cout << endl;
}


/* function to print the seeds and their spreads */
void print_seeds(vector<edge_t> seeds)
{
	int i;
	for (i = 0; i < seeds.size(); i++)
		cout << "Seed = " << seeds[i].index << ", Spread = " << seeds[i].weight << endl;
}



int main(int argc, char *argv[])
{	
	int i, j, sample, batch;					/* counters */
	long time = 0;								/* time counter */
	int number_of_samples;						/* number of times simulation is run */
	int number_of_seeds;						/* number of seeds to return */
	int number_of_vertices = 0;					/* graph parameters */
	double deadline = 0.2;						/* deadline */
	double extended_deadline = deadline * 2;	/* extended deadline for getting paths */
	clock_t start, stop;						/* variables for measuring time taken by the CPU */
	
	char *fileName = argv[1];					/* input file path passed as an argument in console */
	number_of_samples = atoi(argv[2]);			/* no. of iterations passed as argument in console */
	number_of_seeds = atoi(argv[3]);			/* no. of seeds to return passed as an argument in console */

	cout << "Reading graph from file" << endl;
	start = clock();
	read_graph(fileName, &number_of_vertices);
	//print_graph(number_of_vertices);
	stop = clock();
	cout << "Done reading graph from file in " << (double) (stop - start) / CLOCKS_PER_SEC << " seconds\n" << endl;
	time += (double) (stop - start) / CLOCKS_PER_SEC;
	
	/* compute the extended adjacency list by finding the shortest paths within the extended deadline */
	start = clock();
	cout << "Computing shortest paths with deadline = " << deadline << endl;
	vector<edge_t> *S = new (nothrow) vector<edge_t>[number_of_vertices];
	if (S == NULL) {
		cout << "Error: Memory could not be allocated." << endl;
		exit(1);
	}
	compute_extended_adjacency(number_of_vertices, extended_deadline, S);
	//print_similarity(number_of_vertices, S);
	stop = clock();
	cout << "Done computing shortest paths in " << (double) (stop - start) / CLOCKS_PER_SEC << " seconds\n" << endl;
	time += (stop - start) / CLOCKS_PER_SEC;
	double time1 = time;
	
	/* compute the seeds */
	cout << "Computing " << number_of_seeds << " seeds" << endl;	
	rand_val(start);							/* set the seed */
	double total_spread = 0.0;
	vector<edge_t> seeds;						/* collect the seeds */
	double *distance_from_seeds = new double[number_of_vertices];
	if (distance_from_seeds == NULL) {
		cout << "Error: Memory could not be allocated." << endl;
		exit(1);
	}
	for (i = 0; i < number_of_vertices; i++)
		distance_from_seeds[i] = (double) INFINITY;
	
	for (batch = 0; batch < number_of_seeds; batch++) {
		cout << "Computing seed " << batch + 1 << endl;
		start = clock();
		vector<edge_t> *T = new (nothrow) vector<edge_t>[number_of_vertices];
		if (T == NULL) {
			cout << "Error: Memory could not be allocated." << endl;
			exit(1);
		}
		for (i = 0; i < number_of_vertices; i++) {
			for (j = 0; j < S[i].size(); j++) {
				int index = S[i][j].index;
				if (S[i][j].weight < distance_from_seeds[index])
					T[i].push_back(S[i][j]);
			}
		}
		
		double *spread = new (nothrow) double[number_of_vertices];
		if (spread == NULL) {
			cout << "Error: Memory could not be allocated." << endl;
			exit(1);
		}
		for (i = 0; i < number_of_vertices; i++)
			spread[i] = 0.0;
		
		/* run simulation */
		for (sample = 0; sample < number_of_samples; sample++) {
			/* generate the random numbers for the weights on the paths */
			for (i = 0; i < number_of_vertices; i++) {
				if (distance_from_seeds[i] > 1E-10) {
					for (j = 0; j < T[i].size(); j++) {
						int index = T[i][j].index;
						if (distance_from_seeds[index] == INFINITY) {
							double weight = exponential(T[i][j].weight);
							if (weight < deadline)
								spread[i] += 1.0;
		}	}	}	}	}
		
		/* find the node with the maximum spread in this round */
		int max_index;
		double max = - (double) INFINITY;
		for (i = 0; i < number_of_vertices; i++) {
			if (distance_from_seeds[i] > 1E-10) {
				if (spread[i] > max) {
					max = spread[i];
					max_index = i;
		}	}	}
		if (max > -INFINITY) {
			edge_t edge;
			edge.index = max_index;
			edge.weight = max / number_of_samples;
			seeds.push_back(edge);
			cout << "Seed obtained in this round = " << max_index << ", with marginal spread = " << edge.weight << endl;
			total_spread += max / number_of_samples;
			for (j = 0; j < T[max_index].size(); j++) {
				int index = T[max_index][j].index;
				double distance = T[max_index][j].weight;
				if (distance < distance_from_seeds[index])
					distance_from_seeds[index] = distance;
		}	}
		
		stop = clock();
		cout << "Done computing seed " << batch + 1 << " in " << (double) (stop - start) / CLOCKS_PER_SEC << " seconds\n" << endl;
		time += (stop - start) / CLOCKS_PER_SEC;
		
		delete[] T;
		delete[] spread;
	}
	cout << "Done computing " << number_of_seeds << " seeds in " << (time - time1) << " seconds\n" << endl;
	
	/* compute the expected spread of each node */
	cout << "Final Results:" << endl;
	print_seeds(seeds);
	cout << "Total spread = " << total_spread << endl;
	cout << "Total time taken = " << time << " seconds" << endl;
	
	/* free allocated memory */
	delete[] S;
	delete[] distance_from_seeds;
	
	return 0;
}