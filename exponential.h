//=-------------------------------------------------------------------------=
//=  Author: Kenneth J. Christensen                                         =
//=          University of South Florida                                    =
//=          WWW: http://www.csee.usf.edu/~christen                         =
//=          Email: christen@csee.usf.edu                                   =
//=-------------------------------------------------------------------------=


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

//===========================================================================
//= Multiplicative LCG for generating uniform(0.0, 1.0) random numbers      =
//=   - x_n = 7^5*x_(n-1)mod(2^31 - 1)                                      =
//=   - With x seeded to 1 the 10000th x value should be 1043618065         =
//=   - From R. Jain, "The Art of Computer Systems Performance Analysis,"   =
//=     John Wiley & Sons, 1991. (Page 443, Figure 26.2)                    =
//=-------------------------------------------------------------------------=
//= Input: Seed value of positive integer (0 for no seed)                   =
//= Output: Returns unif RV if input is 0                                   =
//===========================================================================
double rand_val(int seed)
{
	const long  a =      16807;    // Multiplier
	const long  m = 2147483647;    // Modulus
	const long  q =     127773;    // m div a
	const long  r =       2836;    // m mod a
	static long x;                 // Random int value that is seeded
	long        x_div_q;           // x divided by q
	long        x_mod_q;           // x modulo q
	long        x_new;             // New x value

	// Seed the RNG if seed is not equal to zero
	if (seed > 0)
		x = seed;

	// RNG using integer arithmetic
	x_div_q = x / q;
	x_mod_q = x % q;
	x_new = (a * x_mod_q) - (r * x_div_q);
	if (x_new > 0)
		x = x_new;
	else
		x = x_new + m;

	// Return a random value between 0.0 and 1.0
	return((double) x / m);
}


//===========================================================================
//=  Function to generate exponentially distributed random variables        =
//=    - Input:  Scale parameter/Mean of exponential distribution           =
//=    - Output: Returns with exponentially distributed random variable     =
//===========================================================================
double exponential(double lambda)
{
	double z;                     // Uniform random number (0 < z < 1)
	double exp_value;             // Computed exponential value to be returned

	// Pull a uniform random number (0 < z < 1)
	do
	{
		z = rand_val(0);
	} while ((z == 0) || (z == 1));

	// Compute exponential random variable using inversion method
	exp_value = -1 * log(z) * lambda;

	return(exp_value);
}


//===========================================================================
//= Main function to test the code                                          =
//===========================================================================
/*int main(int n, char** argv)
{
    int seed = time(NULL);
    rand_val(seed);
    
    double lambda = 1.0;
    for(int i = 0; i < 10; i++) {
        printf("%lf\n", exponential(lambda));
    }
    
    return 0;
}*/
