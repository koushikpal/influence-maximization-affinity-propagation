/****************************************
	Author: Koushik Pal
			Zissis Poulos
****************************************/

#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <ctime>
#include <cmath>
#include <algorithm>
#include <assert.h>
#include "exponential.h"

#define MAX_VERTICES 90000
#define BATCH_SIZE 10000


using namespace std;


typedef struct edge_t edge_t;
typedef struct dijkstra_tree dijkstra_tree;

struct edge_t {
	int index;											/* index of the other vertex of an edge */
	double weight;										/* edge parameter - either mean of the exponential distribution or weight or similarity on the edge */
};

struct dijkstra_tree {
	int index;
	int parent_index;
	int reindex;
	int parent_reindex;
	double edge_mean;
};

vector<edge_t> graph[MAX_VERTICES];						/* the adjacency list of the graph */
bool hash_nodes[MAX_VERTICES];							/* hash map needed to count the number of (unique) vertices in the input graph */



/* function for computing the path lengths in a (Dijkstra) tree */
void compute_path_length_rec(int i, int j, vector<dijkstra_tree> *S, double *sample_weights, double *path_lengths)
{
	if (path_lengths[j] >= 0.0)
		return;
	if (S[i][j].parent_index == i) {
		path_lengths[j] = sample_weights[j];
		return;
	}
	compute_path_length_rec(i, S[i][j].parent_reindex, S, sample_weights, path_lengths);
	path_lengths[j] = sample_weights[j] + path_lengths[S[i][j].parent_reindex];
}


/* function for computing the path lengths in a (Dijkstra) tree */
void compute_path_length(int i, int j, int sample_weights_index, vector<dijkstra_tree> *S, vector<double> *sample_weights, double *path_lengths_sample)
{
	if (path_lengths_sample[j] > -1.0)
		return;
	if (S[i][j].parent_index == i) {
		path_lengths_sample[j] = sample_weights[sample_weights_index][j];
		return;
	}
	
	vector<int> parent_indices;
	while (path_lengths_sample[j] < 0.0 && S[i][j].parent_index != i) {
		parent_indices.push_back(j);
		j = S[i][j].parent_reindex;
	}
	parent_indices.push_back(j);
	
	if (S[i][j].parent_index == i)
		path_lengths_sample[j] = sample_weights[sample_weights_index][j];
	assert(parent_indices.size() >= 2);
	assert(parent_indices.size() < 20);
	for (int k = parent_indices.size() - 2; k >= 0; k--) {
		assert(path_lengths_sample[parent_indices[k + 1]] >= 0.0);
		path_lengths_sample[parent_indices[k]] = path_lengths_sample[parent_indices[k + 1]] + sample_weights[sample_weights_index][parent_indices[k]];
	}
}


/* function to compute the spread of a given node for a given sample */
void compute_spread(int i, int seed, double deadline, double *spread, vector<double> *sample_weights, vector<edge_t> *final_seeds, vector<dijkstra_tree> *S)
{
	int j, k, index;
	vector<int> infected;
	
	for (k = 0; k <= seed; k++) {
		if (k < seed)
			index = (*final_seeds)[k].index;
		else
			index = i;
		
		double *path_lengths_sample = new (nothrow) double[S[index].size()];
		if (path_lengths_sample == NULL) {
			cout << "Error: Memory could not be allocated." << endl;
			exit(1);
		}
		
		for (j = 0; j < S[index].size(); j++)
			path_lengths_sample[j] = -1.0;
		
		for (j = 0; j < S[index].size(); j++) {
			compute_path_length(index, j, k, S, sample_weights, path_lengths_sample);
			assert(path_lengths_sample[j] > -1.0);
			vector<int>::iterator it = find(infected.begin(), infected.end(), j);
			if (path_lengths_sample[j] < deadline && it == infected.end()) {
				spread[i] += 1.0;
				infected.push_back(j);
		}	}
		
		/* free allocated memory */
		delete[] path_lengths_sample;
	}
}


/* function to sample from the Dijkstra trees of all nodes, compute their spread and find seeds */
void compute_seeds(int number_of_vertices, int number_of_seeds, int number_of_samples, double deadline, double *total_spread, vector<edge_t> *final_seeds, vector<dijkstra_tree> *S)
{
	int i, j, k, seed, batch, sample, index;
	clock_t start, stop;
	
	int number_of_batches = (number_of_vertices + BATCH_SIZE - 1) / BATCH_SIZE;
	cout << "Number of batches = " << number_of_batches << ", Number of samples = " << number_of_samples << endl;
	rand_val(clock());							/* set the seed */
	
	bool *is_seed = new (nothrow) bool[number_of_vertices];
	if (is_seed == NULL) {
		cout << "Error: Memory could not be allocated." << endl;
		exit(1);
	}
	for (i = 0; i < number_of_vertices; i++)
		is_seed[i] = false;
	
	for (seed = 0; seed < number_of_seeds; seed++) {
		cout << "\nComputing seed " << seed + 1 << endl;
		start = clock();
		
		double *spread = new (nothrow) double[number_of_vertices];
		if (spread == NULL) {
			cout << "Error: Memory could not be allocated." << endl;
			exit(1);
		}
		for (i = 0; i < number_of_vertices; i++)
			spread[i] = 0.0;
		
		/* sample from edges in the Dijkstra tree and compute spread of all nodes except seeds */
		for (batch = 0; batch < number_of_batches; batch++) {
			cout << "Computing batch " << batch + 1 << endl;
			stop = clock();
			int lower_limit = batch * BATCH_SIZE;
			int upper_limit = (batch + 1) * BATCH_SIZE < number_of_vertices ? (batch + 1) * BATCH_SIZE : number_of_vertices;
			for (i = lower_limit; i < upper_limit; i++) {
				if (!is_seed[i]) {
					for (sample = 0; sample < number_of_samples; sample++) {
						vector<double> *sample_weights = new vector<double>[seed + 1];
						for (k = 0; k <= seed; k++) {
							if (k < seed)
								index = (*final_seeds)[k].index;
							else
								index = i;
							for (j = 0; j < S[index].size(); j++) {
								if (S[index][j].edge_mean < 1E-10)
									sample_weights[k].push_back(0.0);
								else
									sample_weights[k].push_back(exponential(S[index][j].edge_mean));
						}	}
						
						compute_spread(i, seed, deadline, spread, sample_weights, final_seeds, S);
						delete[] sample_weights;
			}	}	}
			cout << "Done computing batch " << batch << " in " << (double) (clock() - stop) / CLOCKS_PER_SEC << " seconds" << endl;
			stop = clock();
		}
		
		int max_index;
		double max = - (double) INFINITY;
		for (i = 0; i < number_of_vertices; i++) {
			if (!is_seed[i]) {
				if (spread[i] > max) {
					max = spread[i];
					max_index = i;
		}	}	}
		if (max > -INFINITY) {
			edge_t edge;
			edge.index = max_index;
			edge.weight = (max / number_of_samples) >= *total_spread ? max / number_of_samples - *total_spread : 0.0;
			(*final_seeds).push_back(edge);
			is_seed[max_index] = true;
			cout << "Seed obtained in this round = " << edge.index << ", with marginal spread = " << edge.weight << endl;
			*total_spread += edge.weight;
		}
		
		stop = clock();
		cout << "Done computing seed " << seed + 1 << " in " << (double) (stop - start) / CLOCKS_PER_SEC << " seconds" << endl;
		
		/* free allocated memory */
		delete[] spread;
	}
	
	/* free allocated memory */
	delete[] is_seed;
}


/* --- priority queue functions --- */
void update_queue(int index, double *shortest_distance, int *heap_index, int *heap, int *heap_length)
{
	int i, j;

	/* find existing heap entry, or create a new one */
	i = heap_index[index];
	if (!i) 
		i = ++(*heap_length);

	/* upheap */
	for (; i > 1 && shortest_distance[index] < shortest_distance[heap[j = i / 2]]; i = j)
		heap_index[heap[i] = heap[j]] = i;

	heap[i] = index;
	heap_index[index] = i;
}


int pop_queue(double *shortest_distance, int *heap_index, int *heap, int *heap_length)
{
	int i, j;
	int index_to_return, tmp_index;

	if (*heap_length == 0)
		return -1;

	/* remove leading element, pull tail element there and downheap */
	index_to_return = heap[1];
	tmp_index = heap[(*heap_length)--];

	for (i = 1; i < *heap_length && (j = i * 2) <= *heap_length; i = j) {
		if (j < *heap_length && shortest_distance[heap[j]] > shortest_distance[heap[j + 1]]) 
			j++;

		if (shortest_distance[heap[j]] >= shortest_distance[tmp_index]) 
			break;
		heap_index[heap[i] = heap[j]] = i;
	}

	heap[i] = tmp_index;
	heap_index[tmp_index] = i;

	return index_to_return;
}


/* function for computing the shortest path using priority queue Dijkstra algorithm */
void dijkstra_heap(int start_index, int number_of_vertices, int number_of_samples, double sigma, double deadline, double *shortest_distance, double *variance, double *parent_edge_mean, int *parent, int *heap_index, int *heap, int *heap_length)
{
	int i;												/* counter */
	int curr_index;										/* index of current vertex to process */
	int next_index;										/* index of candidate next vertex to process */
	double distance;									/* weight of current edge to process */
	
	parent[start_index] = start_index;
	parent_edge_mean[start_index] = 0.0;
	shortest_distance[start_index] = 0.0;
	variance[start_index] = 0.0;
	update_queue(start_index, shortest_distance, heap_index, heap, heap_length);
	
	while ((curr_index = pop_queue(shortest_distance, heap_index, heap, heap_length)) != -1) {
		if (shortest_distance[curr_index] - sigma * sqrt(variance[curr_index]) >= deadline) {
		//if (shortest_distance[curr_index] * (1 - 1.96 / sqrt(number_of_samples)) >= deadline) {
			break;
		}
		for (i = 0; i < graph[curr_index].size(); i++) {
			next_index = graph[curr_index][i].index;
			distance = shortest_distance[curr_index] + graph[curr_index][i].weight;
			if (distance < shortest_distance[next_index]) {
				parent[next_index] = curr_index;
				parent_edge_mean[next_index] = graph[curr_index][i].weight;
				shortest_distance[next_index] = distance;
				variance[next_index] = variance[curr_index] + pow(graph[curr_index][i].weight, 2);
				update_queue(next_index, shortest_distance, heap_index, heap, heap_length);
	}	}	}
}


/* function to compute the Dijkstra trees within deadline for each node of the graph */
void compute_dijkstra_trees(int number_of_vertices, int number_of_samples, double sigma, double deadline, vector<dijkstra_tree> *S)
{
	int i, j;											/* counters */

	/* variables needed for running Dijkstra */
	double *shortest_distance;							/* array containing the shortest distance of each node from the source node */
	double *variance;									/* array containing the variance of the shortest distance of each node from the source node */
	double *parent_edge_mean;							/* array to store the mean on the edge between the node and its parent in the Dijkstra tree */
	int *parent;										/* array containing the parent index of a node in the shortest path tree */
	int *heap_index;									/* index of the node in the heap */
	int *heap;											/* priority queue or min heap for implementing Dijkstra */
	int heap_length;									/* number of elements in the heap */
	int *reindex_map;									/* array to reindex the Dijkstra tree for each node */
	
	shortest_distance = new (nothrow) double[number_of_vertices];
	variance = new (nothrow) double[number_of_vertices];
	parent_edge_mean = new (nothrow) double[number_of_vertices];
	parent = new (nothrow) int[number_of_vertices];
	heap_index = new (nothrow) int[number_of_vertices];
	heap = new (nothrow) int[number_of_vertices + 1];
	reindex_map = new (nothrow) int[number_of_vertices];
	
	/* for each node, find the shortest distance to all other nodes and estimate its total spread */
	for (i = 0; i < number_of_vertices; i++) {
		heap_length = 0;
		for (j = 0; j < number_of_vertices; j++) {
			shortest_distance[j] = (double) INFINITY;
			variance[j] = - (double) INFINITY;
			parent_edge_mean[j] = -1.0;
			parent[j] = -1;
			heap_index[j] = 0;
			heap[j] = -1;
			reindex_map[j] = -1;
		}
	
		dijkstra_heap(i, number_of_vertices, number_of_samples, sigma, deadline, shortest_distance, variance, parent_edge_mean, parent, heap_index, heap, &heap_length);
		
		dijkstra_tree edge;
		edge.index = i;
		edge.parent_index = i;
		edge.edge_mean = 0.0;
		S[i].push_back(edge);
		
		for (j = 0; j < number_of_vertices; j++) {
			if ((shortest_distance[j] - sigma * sqrt(variance[j]) < deadline) && (j != i)) {
			//if (shortest_distance[j] * (1 - 1.96 / sqrt(number_of_samples)) < deadline) {
				edge.index = j;
				edge.parent_index = parent[j];
				assert(parent_edge_mean[j] > -0.5);
				edge.edge_mean = parent_edge_mean[j];
				S[i].push_back(edge);
		}	}
		
		int reindex = 0;
		for (j = 0; j < S[i].size(); j++)
			reindex_map[S[i][j].index] = reindex++;
		
		for (j = 0; j < S[i].size(); j++) {
			assert(reindex_map[S[i][j].index] != -1);
			S[i][j].reindex = reindex_map[S[i][j].index];
			assert(reindex_map[S[i][j].parent_index] != -1);
			S[i][j].parent_reindex = reindex_map[S[i][j].parent_index];
	}	}
	
	unsigned int sim_total_size = 0, sim_total_count = 0, sim_max_count = 0, max_node;
	for (i = 0; i < number_of_vertices; i++) {
		sim_total_size += sizeof(dijkstra_tree) * S[i].size();
		sim_total_count += S[i].size();
		if (S[i].size() > sim_max_count) {
			sim_max_count = S[i].size();
			max_node = i;
	}	}
	cout << "Total size of the Dijkstra trees for all nodes (in MB) = " << (double) sim_total_size / 1000000.0 << endl;
	cout << "Total number of entries in all the Dijkstra trees combined = " << sim_total_count << endl;
	cout << "Maximum number of entries in any of the Dijkstra trees = " << sim_max_count << " for node " << max_node << endl;
	
	/* free allocated memory */
	delete[] shortest_distance;
	delete[] variance;
	delete[] parent_edge_mean;
	delete[] parent;
	delete[] heap_index;
	delete[] heap;
	delete[] reindex_map;
}


/* function to read a graph from a file */
void read_graph(char *fileName_graph, int *number_of_vertices)
{
	int i, u, v, number_of_edges = 0;
	double alpha, beta;
	ifstream infile;
	
	infile.open(fileName_graph, ios::in);
	if (!infile.is_open()) {
		cout << "Error: File not found." << endl;
		exit(1);
	}

	/* parse file and construct adjacency matrix. also extract number of vertices and number of edges. */
	while (infile >> u >> v >> alpha >> beta) {
		if (hash_nodes[u] == 0) {
			(*number_of_vertices)++;
			hash_nodes[u] = 1;
		}
		if (hash_nodes[v] == 0) {
			(*number_of_vertices)++;
			hash_nodes[v] = 1;
		}
		
		edge_t edge;
		edge.index = v;
		edge.weight = alpha * beta;			/* we assume the edge distributions are Gamma(alpha, beta), where alpha is the shape and beta is the scale parameter */
		assert(u < MAX_VERTICES);
		graph[u].push_back(edge);
		number_of_edges++;
	}
	
	long node_total_size = 0, node_total_count = 0, node_max_count = 0, max_node;
	for (i = 0; i < *number_of_vertices; i++) {
		node_total_size += sizeof(edge_t) * graph[i].size();
		node_total_count += graph[i].size();
		if (graph[i].size() > node_max_count) {
			node_max_count = graph[i].size();
			max_node = i;
	}	}
	if (node_total_count != number_of_edges)
		cout << "Warning: Node total count = " << node_total_count << ", number of edges = " << number_of_edges << endl;
	cout << "Total number of vertices = " << *number_of_vertices << endl;
	cout << "Total size of the graph adjacency list (in MB) = " << (double) node_total_size / 1000000.0 << endl;
	cout << "Total number of entries in the graph adjacency list = " << node_total_count << endl;
	cout << "Maximum length of any row in the graph adjacency list = " << node_max_count << " for node " << max_node << endl;
	
	infile.close();
}


/* function to print the adjacency list of the graph */
void print_graph(int number_of_vertices)
{
	int i, j;
	cout << "The following is the adjacency list of the graph:" << endl;
	for (i = 0; i < number_of_vertices; i++) {
		cout << setw(10) << i;
		for(j = 0; j < graph[i].size(); j++)
			cout << setw(10) << graph[i][j].index << " (" << graph[i][j].weight << ") ";
		cout << endl;		
	}
	cout << endl;
}


/* function to print the similarity matrix */
void print_similarity(int number_of_vertices, vector<dijkstra_tree> *S)
{
	int i, j;
	cout << "The following are the dijkstra trees for all nodes of the graph:" << endl;
	for (i = 0; i < number_of_vertices; i++) {
		if (S[i].size() > 0) {
			cout << setw(10) << i << endl;
			for (j = 0; j < S[i].size(); j++)
				cout << setw(10) << S[i][j].index << setw(10) << S[i][j].reindex << setw(10) << S[i][j].parent_index 
						<< setw(10) << S[i][j].parent_reindex << setw(10) << S[i][j].edge_mean << endl;
	}	}
	cout << endl;
}


/* function to print the seeds and their spreads */
void print_seeds(vector<edge_t> seeds)
{
	int i;
	cout << "\nThe following is the list of seeds with their marginal spread:" << endl;
	for (i = 0; i < seeds.size(); i++)
		cout << "Seed = " << seeds[i].index << ", Spread = " << seeds[i].weight << endl;
}



int main(int argc, char *argv[])
{
	double time = 0;							/* time counter */
	int number_of_samples;						/* number of times simulation is run */
	int number_of_seeds;						/* number of seeds to return */
	int number_of_vertices = 0;					/* graph parameters */
	double deadline = 0.2;						/* deadline */
	double extended_deadline = deadline * 1;	/* extended deadline for getting paths */
	double sigma = 0.9;							/* parameter for considering deviation around mean */
	clock_t start, stop;						/* variables for measuring time taken by the CPU */
	
	char *fileName = argv[1];					/* input file path passed as an argument in console */
	number_of_samples = atoi(argv[2]);			/* no. of iterations passed as argument in console */
	number_of_seeds = atoi(argv[3]);			/* no. of seeds to return passed as an argument in console */
	
	cout << "Reading graph from file" << endl;
	start = clock();
	read_graph(fileName, &number_of_vertices);
	//print_graph(number_of_vertices);
	stop = clock();
	cout << "Done reading graph from file in " << (double) (stop - start) / CLOCKS_PER_SEC << " seconds" << endl;
	time += (double) (stop - start) / CLOCKS_PER_SEC;
	
	/* compute the Dijkstra tree for each node by finding the shortest paths to other nodes within the deadline */
	cout << "\nComputing shortest paths with deadline = " << extended_deadline << " and sigma = " << sigma << endl;
	vector<dijkstra_tree> *S = new (nothrow) vector<dijkstra_tree>[number_of_vertices];
	if (S == NULL) {
		cout << "Error: Memory could not be allocated." << endl;
		exit(1);
	}
	start = clock();
	compute_dijkstra_trees(number_of_vertices, number_of_samples, sigma, extended_deadline, S);
	//print_similarity(number_of_vertices, S);
	stop = clock();
	cout << "Done computing shortest paths in " << (double) (stop - start) / CLOCKS_PER_SEC << " seconds" << endl;
	time += (stop - start) / CLOCKS_PER_SEC;
	
	/* compute the seeds */
	number_of_seeds = number_of_seeds < number_of_vertices? number_of_seeds: (number_of_vertices - 1);
	cout << "\nComputing " << number_of_seeds << " seeds" << endl;
	double total_spread = 0.0;
	vector<edge_t> final_seeds;					/* collect the seeds */
	start = clock();
	compute_seeds(number_of_vertices, number_of_seeds, number_of_samples, deadline, &total_spread, &final_seeds, S);
	stop = clock();
	cout << "\nDone computing " << number_of_seeds << " seeds in " << (double) (stop - start) / CLOCKS_PER_SEC << " seconds" << endl;
	time += (double) (stop - start) / CLOCKS_PER_SEC;
	
	/* compute the expected spread of each node */
	cout << "\nFinal Results:" << endl;
	print_seeds(final_seeds);
	cout << "Total spread = " << total_spread << endl;
	cout << "\nTotal time taken = " << time << " seconds" << endl;
	
	/* free allocated memory */
	delete[] S;
	
	return 0;
}