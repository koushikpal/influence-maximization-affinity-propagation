#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "exponential.h" 
 
 
typedef struct node_t node_t, *heap_t;
typedef struct edge_t edge_t;

struct edge_t {
	node_t *nd;	/* target of this edge */
	edge_t *sibling;/* for singly linked list */
	double len;	/* edge cost */
};

struct node_t {
	edge_t *edge;	/* singly linked list of edges */
	node_t *via;	/* where previous node is in shortest path */
	double dist;	/* distance from origining node */
	char name[10];	/* the name (ID) */
	int heap_idx;	/* link to heap position for updating distance */
};

 
#define BLOCK_SIZE (1024 * 32 - 1)
#define N_NODES 5000
#define INFTY 10000


int adj[N_NODES][N_NODES]={0};
double weights[N_NODES][N_NODES]={0};
double log_dist[N_NODES][N_NODES]={0};
double dist[N_NODES][N_NODES]={0};
//double max_dist[N_NODES]={0}; //[N_NODES] = {0};
double max=0.0;
double sim[N_NODES][N_NODES]={0};
double g[N_NODES]={INFTY};
double total_spread[N_NODES]={0};
double spread[N_NODES]={0};
double avg_spread[N_NODES]={0};
edge_t *edge_root = 0, *e_next = 0;
 
/* Function to add edge between nodes*/
void add_edge(node_t *a, node_t *b, double d)
{
	if (e_next == edge_root) {
		edge_root = malloc(sizeof(edge_t) * (BLOCK_SIZE + 1));
		edge_root[BLOCK_SIZE].sibling = e_next;
		e_next = edge_root + BLOCK_SIZE;
	}
	--e_next;
 
	e_next->nd = b;
	e_next->len = d;
	e_next->sibling = a->edge;
	a->edge = e_next;
}

/*use this to free edges when done*/ 
void free_edges()
{
	for (; edge_root; edge_root = e_next) {
		e_next = edge_root[BLOCK_SIZE].sibling;
		free(edge_root);
	}
}

/* use to print adjacency matrix*/
void printA(int n) {
	int i,j;
	for (i = 1; i <= n; ++i){
		for (j = 1; j <= n; ++j){
			printf("%d", adj[i][j]);
			/*printf("\t");*/
		}
	printf("\n");		
	}
printf("\n");
}

 
/* --- priority queue functions --- */
heap_t *heap;
int heap_len;
 
void set_dist(node_t *nd, node_t *via, double d)
{
	int i, j;
 
	/* already knew better path */
	if (nd->via && d >= nd->dist) return;
 
	/* find existing heap entry, or create a new one */
	nd->dist = d;
	nd->via = via;
 
	i = nd->heap_idx;
	if (!i) i = ++heap_len;
 
	/* upheap */
	for (; i > 1 && nd->dist < heap[j = i/2]->dist; i = j)
		(heap[i] = heap[j])->heap_idx = i;
 
	heap[i] = nd;
	nd->heap_idx = i;
}
 
node_t * pop_queue()
{
	node_t *nd, *tmp;
	int i, j;
 
	if (!heap_len) return 0;
 
	/* remove leading element, pull tail element there and downheap */
	nd = heap[1];
	tmp = heap[heap_len--];
 
	for (i = 1; i < heap_len && (j = i * 2) <= heap_len; i = j) {
		if (j < heap_len && heap[j]->dist > heap[j+1]->dist) j++;
 
		if (heap[j]->dist >= tmp->dist) break;
		(heap[i] = heap[j])->heap_idx = i;
	}
 
	heap[i] = tmp;
	tmp->heap_idx = i;
 
	return nd;
}
 
/* --- Dijkstra main body. Unreachable nodes will never make into the queue --- */
void calc_all(node_t *start)
{
	node_t *lead;
	edge_t *e;
 
	set_dist(start, start, 0);
	while ((lead = pop_queue()))
		for (e = lead->edge; e; e = e->sibling)
			set_dist(e->nd, lead, lead->dist + e->len);
}

/* use this if you want to print the actual shortest paths. Recursive*/ 
void show_path(node_t *nd)
{
	if (nd->via == nd)
		printf("%s", nd->name);
	else if (!nd->via)
		printf("%s(unreached)", nd->name);
	else {
		show_path(nd->via);
		printf("-> %s(%g) ", nd->name, nd->dist);
	}
}

/* count shortest paths that are within deadline t*/

int countS(double D[], double t, int n){
	int i, path_count = 0;
	for (i = 0; i < n; ++i){
		if (D[i] <= t)
			path_count++;
	}
	return path_count;
}

double floydWarshell (int num_vertices)
{
    /* dist[][] will be the output matrix that will finally have the shortest 
      distances between every pair of vertices */
    int i, j, k;
double max=0.0;
 
    /* Initialize the solution matrix same as input graph matrix. Or 
       we can say the initial values of shortest distances are based
       on shortest paths considering no intermediate vertex. */
	    for (i = 1; i <= num_vertices; i++)
        	for (j = 1; j <= num_vertices; j++)
	    	if (log_dist[i][j] == 0.0)
			dist[i][j] = INFTY;
			else
            		dist[i][j] = log_dist[i][j];
 
    /* Add all vertices one by one to the set of intermediate vertices.
      ---> Before start of a iteration, we have shortest distances between all
      pairs of vertices such that the shortest distances consider only the
      vertices in set {0, 1, 2, .. k-1} as intermediate vertices.
      ----> After the end of a iteration, vertex no. k is added to the set of
      intermediate vertices and the set becomes {0, 1, 2, .. k} */
    	for (k = 1; k <= num_vertices; k++)
    	{
        // Pick all vertices as source one by one
        	for (i = 1; i <= num_vertices; i++)
        	{
            // Pick all vertices as destination for the
            // above picked source
        	    	for (j = 1; j <= num_vertices; j++)
            		{
                // If vertex k is on the shortest path from
                // i to j, then update the value of dist[i][j]
                	if (dist[i][k] + dist[k][j] < dist[i][j])
                    	dist[i][j] = dist[i][k] + dist[k][j];
            		}
        	}
    	}
	
	for (j=1; j<=num_vertices; j++){
		for (i=1; i<=num_vertices; i++){
		//	if ((max_dist[j] < dist[i][j]) && (dist[i][j]!=INFTY))
		//		max_dist[j] = dist[i][j];
			if ((max < dist[i][j]) && (dist[i][j]!=INFTY))
				max = dist[i][j];

		}
	}
return max;			
}
 
int main(int argc, char *argv[])
{
	int i, j, k, E, V, N, lambda, sample, my_seed;
	int max_j, max_k, u, v;
	int node[N_NODES]={0};
	double exp, T, w;
	
	
	
	FILE *fin = fopen(argv[1], "r");
	FILE *fout = fopen(argv[2], "w");
	FILE *fout2 = fopen(argv[3], "w");  
 	fscanf(fin, "%d", &E);    //first row of file is #edges
			    
	V = -1;
	max_k=0;

	/*parse file and construct adjacency matrix d[][]. Also extracts the # of vertices V and unique IDs*/

	for (i = 0; i < E; ++i) {
		k=1;
		j=1;
					
		fscanf(fin, "%d%d%lf", &u, &v, &w);
		while (u != node[j] && j<=max_k){
			++j;	
		}
		node[j] = u;
		if (j>max_k)
			max_k=j;
			
		while (v != node[k] && k<=max_k ){
			++k;	
		}	
		node[k] = v;
		if (k>max_k)
			max_k=k;
		
		adj[u][v] = 1;
		weights[u][v] = w;
		log_dist[u][v] = -log(w); 
		V = max_k;
	}
	fclose(fin);
//	V=V-1;
//	printf("V is %d\n", V );
	for (i=1; i<=V; i++){
		for (j=1; j<=V; j++){
			if (j==1) 
			fprintf(fout, "%d", adj[i][j]);
			else
			fprintf(fout, ", %d", adj[i][j]); 
		}
		fprintf(fout, "\n");
	}

	fprintf(fout, "\n\n");

	for (i=1; i<=V; i++){
		for (j=1; j<=V; j++){
			if (j==1) 
			fprintf(fout, "%lf", weights[i][j]);
			else
			fprintf(fout, ", %lf", weights[i][j]); 
		}
		fprintf(fout, "\n");
	}

	fprintf(fout, "\n\n");

	for (i=1; i<=V; i++){
		for (j=1; j<=V; j++){
			if (j==1) 
			fprintf(fout, "%lf", log_dist[i][j]);
			else
			fprintf(fout, ", %lf", log_dist[i][j]); 
		}
		fprintf(fout, "\n");
	}


	fclose(fout);

	max=floydWarshell(V);

	for (i=1; i<=V; i++){
		for (j=1; j<=V; j++){
			if ((dist[i][j]  == INFTY) && (dist[j][i] != INFTY))
			dist[i][j] = 200*max; // max_dist[j];
			if ((dist[j][i]  == INFTY) && (dist[i][j] != INFTY))
			dist[j][i] = 200*max; //max_dist[i]; 
 		}
	}

	for (i=1; i<=V; i++){
		for (j=1; j<=V; j++){
			if (i==j){
				sim[i][j] = 0.0;
			}
			else{
			sim[i][j] = -dist[i][j];
			}
			fprintf(fout2, "%lf\n", sim[i][j]); 
 		}
	}
	fclose(fout2);

}
