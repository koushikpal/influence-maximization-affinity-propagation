/****************************************
	Author: Koushik Pal
			Zissis Poulos
****************************************/

#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <ctime>
#include <cmath>
#include <assert.h>
#include <curand.h>
#include <curand_kernel.h>
#include "exponential.h"

#define CUDA_CALL(x)\
{\
    const cudaError_t a = (x);\
    if (a != cudaSuccess) {\
        cout << "\nCUDA Error: " << cudaGetErrorString(a) << " (err_num = " << a << ")" << endl;\
        cudaDeviceReset();\
        assert(0);\
    }\
}

#define MAX_VERTICES 90000
#define MAX_DEPTH 30
#define NODES_BATCH_SIZE 90000
#define SAMPLES_BATCH_SIZE 500
#define NUMBER_OF_THREADS_PER_BLOCK 256


using namespace std;


typedef struct edge_t edge_t;
typedef struct dijkstra_tree dijkstra_tree;

struct edge_t {
	int index;											/* index of the other vertex of an edge */
	double weight;										/* edge parameter - either mean of the exponential distribution or weight or similarity on the edge */
};

struct dijkstra_tree {
	int index;
	int parent_index;
	int reindex;
	int parent_reindex;
	double edge_mean;
};

vector<edge_t> graph[MAX_VERTICES];						/* the adjacency list of the graph */
bool hash_nodes[MAX_VERTICES];							/* hash map needed to count the number of (unique) vertices in the input graph */



/* device function to compute exponential random variable */
__device__ double exponential_GPU(double lambda, curandState *local_state)
{
	double z;                     // Uniform random number (0 < z < 1)
	double exp_value;             // Computed exponential value to be returned
	
	// Pull a uniform random number (0 < z < 1)
	do {
		z = curand_uniform_double(local_state);
	} while ((z < 1E-10) || (z > 1.0 - 1E-10));
	assert(z >= 1E-10 && z <= 1.0 - 1E-10);
	assert(log(z) < 0.0);
	
	// Compute exponential random variable using inversion method
	exp_value = -1.0 * log(z) * lambda;
	assert(exp_value >= 0.0);
	
	return exp_value;
}


/* device function for computing the path lengths in a (Dijkstra) tree */
__device__ void compute_path_length(int tid, int j, int max_batch_size, double *dev_path_lengths_sample, int *dev_parent_indices, double *dev_sample_weights, int *dev_starting_points, int *dev_sizes, dijkstra_tree *dev_flatten_S)
{
	assert(dev_starting_points[tid] + j < max_batch_size);
	if (dev_path_lengths_sample[dev_starting_points[tid] + j] > -1.0)
		return;
	if (dev_flatten_S[dev_starting_points[tid] + j].parent_reindex == 0) {
		dev_path_lengths_sample[dev_starting_points[tid] + j] = dev_sample_weights[dev_starting_points[tid] + j];
		return;
	}
	
	int index = 0;
	while (dev_path_lengths_sample[dev_starting_points[tid] + j] < 0.0 && dev_flatten_S[dev_starting_points[tid] + j].parent_reindex != 0) {
		assert(index < MAX_DEPTH);
		assert(tid * MAX_DEPTH + index < NODES_BATCH_SIZE * MAX_DEPTH);
		dev_parent_indices[tid * MAX_DEPTH + index] = j;
		index++;
		assert(dev_starting_points[tid] + j < max_batch_size);
		j = dev_flatten_S[dev_starting_points[tid] + j].parent_reindex;
		assert(dev_starting_points[tid] + j < max_batch_size);
	}
	assert(index < MAX_DEPTH);
	assert(tid * MAX_DEPTH + index < NODES_BATCH_SIZE * MAX_DEPTH);
	dev_parent_indices[tid * MAX_DEPTH + index] = j;
	index++;
	
	assert(dev_starting_points[tid] + j < max_batch_size);
	if (dev_flatten_S[dev_starting_points[tid] + j].parent_reindex == 0)
		dev_path_lengths_sample[dev_starting_points[tid] + j] = dev_sample_weights[dev_starting_points[tid] + j];
	assert(index >= 2);
	for (int k = index - 2; k >= 0; k--) {
		assert((tid * MAX_DEPTH + (k + 1)) < NODES_BATCH_SIZE * MAX_DEPTH);
		assert(dev_starting_points[tid] + dev_parent_indices[tid * MAX_DEPTH + k] < max_batch_size);
		assert(dev_starting_points[tid] + dev_parent_indices[tid * MAX_DEPTH + (k + 1)] < max_batch_size);
		assert(dev_path_lengths_sample[dev_starting_points[tid] + dev_parent_indices[tid * MAX_DEPTH + (k + 1)]] >= 0.0);
		dev_path_lengths_sample[dev_starting_points[tid] + dev_parent_indices[tid * MAX_DEPTH + k]] = dev_path_lengths_sample[dev_starting_points[tid] + dev_parent_indices[tid * MAX_DEPTH + (k + 1)]] + dev_sample_weights[dev_starting_points[tid] + dev_parent_indices[tid * MAX_DEPTH + k]];
	}
}


/* kernel to compute the spread of all nodes across a batch of samples */
__global__ void sample_and_compute_spread(int current_number_of_vertices, int total_size_batch, int max_batch_size, int number_of_blocks, double deadline, bool *dev_within_reach_sample_batch, double *dev_path_lengths_sample, int *dev_parent_indices, double *dev_sample_weights, int *dev_starting_points, int *dev_sizes, dijkstra_tree *dev_flatten_S, curandState *dev_state)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	int j, sample;
	
	while (tid < current_number_of_vertices) {
		for (sample = 0; sample < SAMPLES_BATCH_SIZE; sample++) {
			for (j = 0; j < dev_sizes[tid]; j++) {
				assert(tid < current_number_of_vertices);
				assert(dev_starting_points[tid] + j < max_batch_size);
				assert(dev_flatten_S[dev_starting_points[tid] + j].reindex == j);
				assert(sample * total_size_batch + dev_starting_points[tid] + j < max_batch_size * SAMPLES_BATCH_SIZE);
				
				dev_within_reach_sample_batch[sample * total_size_batch + dev_starting_points[tid] + j] = false;
				dev_path_lengths_sample[dev_starting_points[tid] + j] = -1.0;
				if (dev_flatten_S[dev_starting_points[tid] + j].edge_mean < 1E-10)
					dev_sample_weights[dev_starting_points[tid] + j] = 0.0;
				else {
					assert(tid < number_of_blocks * NUMBER_OF_THREADS_PER_BLOCK);
					dev_sample_weights[dev_starting_points[tid] + j] = exponential_GPU(dev_flatten_S[dev_starting_points[tid] + j].edge_mean, &dev_state[tid]);
					assert(dev_sample_weights[dev_starting_points[tid] + j] >= 0.0);
			}	}
			
			for (j = 0; j < dev_sizes[tid]; j++) {
				compute_path_length(tid, j, max_batch_size, dev_path_lengths_sample, dev_parent_indices, dev_sample_weights, dev_starting_points, dev_sizes, dev_flatten_S);
				assert(dev_starting_points[tid] + j < max_batch_size);
				assert(dev_path_lengths_sample[dev_starting_points[tid] + j] >= 0.0);
				if (dev_path_lengths_sample[dev_starting_points[tid] + j] < deadline) {
					assert(sample * total_size_batch + dev_starting_points[tid] + j < max_batch_size * SAMPLES_BATCH_SIZE);
					dev_within_reach_sample_batch[sample * total_size_batch + dev_starting_points[tid] + j] = true;
		}	}	}
		
		tid += blockDim.x * gridDim.x;
	}
}


/* device function for computing the path lengths in a (Dijkstra) tree for seeds */
__device__ void compute_path_length_for_seeds(int tid, int seed, int j, int number_of_seeds, int max_tree_size, double *dev_path_lengths_sample_for_seeds, int *dev_parent_indices_for_seeds, double *dev_sample_weights_for_seeds, dijkstra_tree *dev_final_seeds_tree)
{
	assert(seed * max_tree_size + j < number_of_seeds * max_tree_size);
	assert(tid * max_tree_size + j < max_tree_size * SAMPLES_BATCH_SIZE);
	if (dev_path_lengths_sample_for_seeds[tid * max_tree_size + j] > -1.0)
		return;
	if (dev_final_seeds_tree[seed * max_tree_size + j].parent_reindex == 0) {
		dev_path_lengths_sample_for_seeds[tid * max_tree_size + j] = dev_sample_weights_for_seeds[tid * max_tree_size + j];
		return;
	}
	
	int index = 0;
	while (dev_path_lengths_sample_for_seeds[tid * max_tree_size + j] < 0.0 && dev_final_seeds_tree[seed * max_tree_size + j].parent_reindex != 0) {
		assert(tid * MAX_DEPTH + index < SAMPLES_BATCH_SIZE * MAX_DEPTH);
		dev_parent_indices_for_seeds[tid * MAX_DEPTH + index] = j;
		index++;
		assert(seed * max_tree_size + j < number_of_seeds * max_tree_size);
		j = dev_final_seeds_tree[seed * max_tree_size + j].parent_reindex;
		assert(seed * max_tree_size + j < number_of_seeds * max_tree_size);
	}
	assert(tid * MAX_DEPTH + index < SAMPLES_BATCH_SIZE * MAX_DEPTH);
	dev_parent_indices_for_seeds[tid * MAX_DEPTH + index] = j;
	index++;
	
	assert(seed * max_tree_size + j < number_of_seeds * max_tree_size);
	assert(tid * max_tree_size + j < max_tree_size * SAMPLES_BATCH_SIZE);
	if (dev_final_seeds_tree[seed * max_tree_size + j].parent_reindex == 0)
		dev_path_lengths_sample_for_seeds[tid * max_tree_size + j] = dev_sample_weights_for_seeds[tid * max_tree_size + j];
	assert(index >= 2);
	for (int k = index - 2; k >= 0; k--) {
		assert((tid * MAX_DEPTH + (k + 1)) < SAMPLES_BATCH_SIZE * MAX_DEPTH);
		assert(tid * max_tree_size + dev_parent_indices_for_seeds[tid * MAX_DEPTH + k] < max_tree_size * SAMPLES_BATCH_SIZE);
		assert(tid * max_tree_size + dev_parent_indices_for_seeds[tid * MAX_DEPTH + (k + 1)] < max_tree_size * SAMPLES_BATCH_SIZE);
		assert(dev_path_lengths_sample_for_seeds[tid * max_tree_size + dev_parent_indices_for_seeds[tid * MAX_DEPTH + (k + 1)]] >= 0.0);
		dev_path_lengths_sample_for_seeds[tid * max_tree_size + dev_parent_indices_for_seeds[tid * MAX_DEPTH + k]] = dev_path_lengths_sample_for_seeds[tid * max_tree_size + dev_parent_indices_for_seeds[tid * MAX_DEPTH + (k + 1)]] + dev_sample_weights_for_seeds[tid * max_tree_size + dev_parent_indices_for_seeds[tid * MAX_DEPTH + k]];
	}
}


/* function to compute the spread of the seeds across a set of samples */
__global__ void compute_seed_spread(int number_of_vertices, int number_of_seeds, int current_number_of_seeds, int max_batch_size, int max_tree_size, int number_of_blocks_for_seeds, double deadline, double *dev_sample_weights_for_seeds, double *dev_path_lengths_sample_for_seeds, int *dev_parent_indices_for_seeds, bool *dev_is_infected_by_seeds, dijkstra_tree *dev_final_seeds_tree, curandState *dev_state_for_seeds)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	int i, j, index, seed;
	
	while (tid < SAMPLES_BATCH_SIZE) {
		for (i = 0; i < number_of_vertices; i++) {
			assert(tid * number_of_vertices + i < number_of_vertices * SAMPLES_BATCH_SIZE);
			dev_is_infected_by_seeds[tid * number_of_vertices + i] = false;
		}
		
		for (seed = 0; seed < current_number_of_seeds; seed++) {
			assert(seed * max_tree_size < number_of_seeds * max_tree_size);
			index = dev_final_seeds_tree[seed * max_tree_size + 0].index;
			assert(tid * number_of_vertices + index < number_of_vertices * SAMPLES_BATCH_SIZE);
			dev_is_infected_by_seeds[tid * number_of_vertices + index] = true;
			
			j = 0;
			while (dev_final_seeds_tree[seed * max_tree_size + j].index != -1) {
				assert(tid * max_tree_size + j < max_tree_size * SAMPLES_BATCH_SIZE);
				dev_path_lengths_sample_for_seeds[tid * max_tree_size + j] = -1.0;
				if (dev_final_seeds_tree[seed * max_tree_size + j].edge_mean < 1E-10)
					dev_sample_weights_for_seeds[tid * max_tree_size + j] = 0.0;
				else {
					assert(tid < number_of_blocks_for_seeds * NUMBER_OF_THREADS_PER_BLOCK);
					dev_sample_weights_for_seeds[tid * max_tree_size + j] = exponential_GPU(dev_final_seeds_tree[seed * max_tree_size + j].edge_mean, &dev_state_for_seeds[tid]);
					assert(dev_sample_weights_for_seeds[tid * max_tree_size + j] >= 0.0);
				}
				j++;
			}
			
			j = 0;
			while (dev_final_seeds_tree[seed * max_tree_size + j].index != -1) {
				compute_path_length_for_seeds(tid, seed, j, number_of_seeds, max_tree_size, dev_path_lengths_sample_for_seeds, dev_parent_indices_for_seeds, dev_sample_weights_for_seeds, dev_final_seeds_tree);
				assert(tid * max_tree_size + j < max_tree_size * SAMPLES_BATCH_SIZE);
				assert(dev_path_lengths_sample_for_seeds[tid * max_tree_size + j] >= 0.0);
				if (dev_path_lengths_sample_for_seeds[tid * max_tree_size + j] < deadline) {
					index = dev_final_seeds_tree[seed * max_tree_size + j].index;
					assert(tid * number_of_vertices + index < number_of_vertices * SAMPLES_BATCH_SIZE);
					dev_is_infected_by_seeds[tid * number_of_vertices + index] = true;
				}
				j++;
		}	}
		
		tid += blockDim.x * gridDim.x;
	}
}


/* kernel to initialize the seeds of the random number generators for all threads */
__global__ void init_seeds(curandState *dev_state, long seed)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	curand_init(seed, tid, 0, &dev_state[tid]);
}


/* function to sample from the Dijkstra trees of all nodes, compute their spread and find seeds */
void compute_seeds(int number_of_vertices, int number_of_seeds, int number_of_samples, double deadline, double sigma, double *total_spread, vector<edge_t> *final_seeds, vector<dijkstra_tree> *S)
{
	int i, j, seed, batch, sample, sample_batch, current_number_of_vertices;
	clock_t start, stop;
	
	int number_of_batches = (number_of_vertices + NODES_BATCH_SIZE - 1) / NODES_BATCH_SIZE;
	int number_of_blocks = (NODES_BATCH_SIZE + NUMBER_OF_THREADS_PER_BLOCK - 1) / NUMBER_OF_THREADS_PER_BLOCK;
	int number_of_blocks_for_seeds = (SAMPLES_BATCH_SIZE + NUMBER_OF_THREADS_PER_BLOCK - 1) / NUMBER_OF_THREADS_PER_BLOCK;
	int number_of_sample_batches = (number_of_samples + SAMPLES_BATCH_SIZE - 1) / SAMPLES_BATCH_SIZE;
	cout << "Number of batches = " << number_of_batches << ", Number of Blocks = " << number_of_blocks << ", Number of samples = " << number_of_samples << ", Number of sample batches = " << number_of_sample_batches << endl;
	
	/* preprocess batch and flattened graph data */
	int total_size_all_trees, total_size_batch, max_batch_size = -1, max_tree_size = -1;
	int *starting_points_in_all_trees = new (nothrow) int[number_of_vertices];
	int *total_size_of_batch = new (nothrow) int[number_of_batches];
	int *cumulative_size_of_batch = new (nothrow) int[number_of_batches];
	int *number_of_vertices_in_batch = new (nothrow) int[number_of_batches];
	int **starting_points_in_batch = new (nothrow) int *[number_of_batches];
	int **sizes_in_batch = new (nothrow) int *[number_of_batches];
	dijkstra_tree **flatten_S_batch = new (nothrow) dijkstra_tree *[number_of_batches];
	
	if (starting_points_in_all_trees == NULL || total_size_of_batch == NULL || cumulative_size_of_batch == NULL || number_of_vertices_in_batch == NULL || starting_points_in_batch == NULL || sizes_in_batch == NULL || flatten_S_batch == NULL) {
		cout << "Error: Memory could not be allocated." << endl;
		exit(1);
	}
	
	total_size_all_trees = 0;
	for (batch = 0; batch < number_of_batches; batch++) {
		number_of_vertices_in_batch[batch] = ((batch + 1) * NODES_BATCH_SIZE <= number_of_vertices) ? NODES_BATCH_SIZE : (number_of_vertices - batch * NODES_BATCH_SIZE);
		cout << "Number of nodes in batch " << batch + 1 << " = " << number_of_vertices_in_batch[batch] << endl;
		if (number_of_batches == 1)
			assert(number_of_vertices_in_batch[0] == number_of_vertices);
		
		starting_points_in_batch[batch] = new (nothrow) int[number_of_vertices_in_batch[batch]];
		sizes_in_batch[batch] = new (nothrow) int[number_of_vertices_in_batch[batch]];
		if (starting_points_in_batch[batch] == NULL || sizes_in_batch[batch] == NULL) {
			cout << "Error: Memory could not be allocated." << endl;
			exit(1);
		}
		
		total_size_batch = 0;
		for (i = 0; i < number_of_vertices_in_batch[batch]; i++) {
			starting_points_in_batch[batch][i] = total_size_batch;
			sizes_in_batch[batch][i] = S[batch * NODES_BATCH_SIZE + i].size();
			total_size_batch += sizes_in_batch[batch][i];
			if (sizes_in_batch[batch][i] > max_tree_size)
				max_tree_size = sizes_in_batch[batch][i];
			
			starting_points_in_all_trees[batch * NODES_BATCH_SIZE + i] = total_size_all_trees;
			total_size_all_trees += S[batch * NODES_BATCH_SIZE + i].size();
		}
		total_size_of_batch[batch] = total_size_batch;
		cout << "Total size of batch " << batch + 1 << " = " << total_size_batch << endl;
		
		if (total_size_batch > max_batch_size)
			max_batch_size = total_size_batch;
		
		if (batch == 0)
			cumulative_size_of_batch[batch] = 0;
		else
			cumulative_size_of_batch[batch] = cumulative_size_of_batch[batch - 1] + total_size_of_batch[batch - 1];
		
		flatten_S_batch[batch] = new (nothrow) dijkstra_tree[total_size_batch];
		if (flatten_S_batch[batch] == NULL) {
			cout << "Error: Memory could not be allocated." << endl;
			exit(1);
		}
		for (i = 0; i < number_of_vertices_in_batch[batch]; i++) {
			for (j = 0; j < sizes_in_batch[batch][i]; j++)
				flatten_S_batch[batch][starting_points_in_batch[batch][i] + j] = S[batch * NODES_BATCH_SIZE + i][j];
	}	}
	cout << "Total size of all trees = " << total_size_all_trees << endl;
	cout << "Size of maximum tree = " << max_tree_size << endl;
	max_tree_size++;
	
	int *spread = new (nothrow) int[number_of_vertices];
	bool *is_seed = new (nothrow) bool[number_of_vertices];
	bool *is_infected_by_seeds = new (nothrow) bool[number_of_vertices * number_of_samples];
	dijkstra_tree *final_seeds_tree = new (nothrow) dijkstra_tree[number_of_seeds * max_tree_size];
	if (spread == NULL || is_seed == NULL || is_infected_by_seeds == NULL || final_seeds_tree == NULL) {
		cout << "Error: Big memory could not be allocated." << endl;
		exit(1);
	}
	for (i = 0; i < number_of_vertices; i++)
		is_seed[i] = false;
	for (i = 0; i < number_of_vertices * number_of_samples; i++)
		is_infected_by_seeds[i] = false;
	for (i = 0; i < number_of_seeds * max_tree_size; i++)
		final_seeds_tree[i].index = -1;
	
	/* define GPU variables */
	int *dev_starting_points, *dev_sizes, *dev_parent_indices, *dev_parent_indices_for_seeds;
	double *dev_sample_weights, *dev_path_lengths_sample, *dev_sample_weights_for_seeds, *dev_path_lengths_sample_for_seeds;
	bool *dev_within_reach_sample_batch, *dev_is_infected_by_seeds;
	dijkstra_tree *dev_flatten_S, *dev_final_seeds_tree;
	curandState *dev_state, *dev_state_for_seeds;
	
	/* allocate memory on the GPU */
	CUDA_CALL(cudaMalloc((void**)&dev_starting_points, sizeof(int) * NODES_BATCH_SIZE));
	CUDA_CALL(cudaMalloc((void**)&dev_sizes, sizeof(int) * NODES_BATCH_SIZE));
	CUDA_CALL(cudaMalloc((void**)&dev_flatten_S, sizeof(dijkstra_tree) * max_batch_size));
	CUDA_CALL(cudaMalloc((void**)&dev_sample_weights, sizeof(double) * max_batch_size));
	CUDA_CALL(cudaMalloc((void**)&dev_within_reach_sample_batch, sizeof(bool) * max_batch_size * SAMPLES_BATCH_SIZE));
	CUDA_CALL(cudaMalloc((void**)&dev_path_lengths_sample, sizeof(double) * max_batch_size));
	CUDA_CALL(cudaMalloc((void**)&dev_parent_indices, sizeof(int) * NODES_BATCH_SIZE * MAX_DEPTH));
	CUDA_CALL(cudaMalloc((void**)&dev_is_infected_by_seeds, sizeof(bool) * number_of_vertices * SAMPLES_BATCH_SIZE));
	CUDA_CALL(cudaMalloc((void**)&dev_final_seeds_tree, sizeof(dijkstra_tree) * number_of_seeds * max_tree_size));
	CUDA_CALL(cudaMalloc((void**)&dev_sample_weights_for_seeds, sizeof(double) * max_tree_size * SAMPLES_BATCH_SIZE));
	CUDA_CALL(cudaMalloc((void**)&dev_path_lengths_sample_for_seeds, sizeof(double) * max_tree_size * SAMPLES_BATCH_SIZE));
	CUDA_CALL(cudaMalloc((void**)&dev_parent_indices_for_seeds, sizeof(int) * SAMPLES_BATCH_SIZE * MAX_DEPTH));
	
	/* initialize number of blocks and GPU random number generators for all threads */
	CUDA_CALL(cudaMemcpy(dev_final_seeds_tree, final_seeds_tree, sizeof(dijkstra_tree) * number_of_seeds * max_tree_size, cudaMemcpyHostToDevice));
	cudaDeviceSynchronize();
	CUDA_CALL(cudaMalloc(&dev_state, number_of_blocks * NUMBER_OF_THREADS_PER_BLOCK));
	init_seeds<<<number_of_blocks, NUMBER_OF_THREADS_PER_BLOCK>>>(dev_state, clock());
	cudaDeviceSynchronize();
	CUDA_CALL(cudaMalloc(&dev_state_for_seeds, number_of_blocks_for_seeds * NUMBER_OF_THREADS_PER_BLOCK));
	init_seeds<<<number_of_blocks_for_seeds, NUMBER_OF_THREADS_PER_BLOCK>>>(dev_state_for_seeds, clock());
	cudaDeviceSynchronize();
	
	
	for (seed = 0; seed < number_of_seeds; seed++) {
		cout << "\nComputing seed " << seed + 1 << endl;
		start = clock();
		
		/* compute the joint spread of each node with the seeds across all iterations */
		for (i = 0; i < number_of_vertices; i++)
			spread[i] = 0;
		
		if (seed > 0) {
			CUDA_CALL(cudaMemcpy(dev_final_seeds_tree, final_seeds_tree, sizeof(dijkstra_tree) * seed * max_tree_size, cudaMemcpyHostToDevice));
			int offset = 0;
			for (i = 0; i < number_of_sample_batches; i++) {
				compute_seed_spread<<<number_of_blocks_for_seeds, NUMBER_OF_THREADS_PER_BLOCK>>>(number_of_vertices, number_of_seeds, seed, max_batch_size, max_tree_size, number_of_blocks_for_seeds, deadline, dev_sample_weights_for_seeds, dev_path_lengths_sample_for_seeds, dev_parent_indices_for_seeds, dev_is_infected_by_seeds, dev_final_seeds_tree, dev_state_for_seeds);
				CUDA_CALL(cudaMemcpy(is_infected_by_seeds + offset, dev_is_infected_by_seeds, sizeof(bool) * number_of_vertices * SAMPLES_BATCH_SIZE, cudaMemcpyDeviceToHost));
				offset += number_of_vertices * SAMPLES_BATCH_SIZE;
		}	}
		
		stop = clock();
		for (batch = 0; batch < number_of_batches; batch++) {
			cout << "Computing batch " << batch + 1 << endl;
			current_number_of_vertices = number_of_vertices_in_batch[batch];
			total_size_batch = total_size_of_batch[batch];
			
			bool *within_reach_sample_batch = new (nothrow) bool[total_size_batch * SAMPLES_BATCH_SIZE];
			if (within_reach_sample_batch == NULL) {
				cout << "Error: Big memory could not be allocated." << endl;
				exit(1);
			}
			
			/* transfer data to the GPU */
			CUDA_CALL(cudaMemcpy(dev_starting_points, starting_points_in_batch[batch], sizeof(int) * current_number_of_vertices, cudaMemcpyHostToDevice));
			CUDA_CALL(cudaMemcpy(dev_sizes, sizes_in_batch[batch], sizeof(int) * current_number_of_vertices, cudaMemcpyHostToDevice));
			CUDA_CALL(cudaMemcpy(dev_flatten_S, flatten_S_batch[batch], sizeof(dijkstra_tree) * total_size_batch, cudaMemcpyHostToDevice));
			
			for (sample_batch = 0; sample_batch < number_of_sample_batches; sample_batch++) {
				cout << "sample batch = " << sample_batch + 1 << endl;
				
				sample_and_compute_spread<<<number_of_blocks, NUMBER_OF_THREADS_PER_BLOCK>>>(current_number_of_vertices, total_size_batch, max_batch_size, number_of_blocks, deadline, dev_within_reach_sample_batch, dev_path_lengths_sample, dev_parent_indices, dev_sample_weights, dev_starting_points, dev_sizes, dev_flatten_S, dev_state);
				
				if (sample_batch > 0) {
					for (sample = 0; sample < SAMPLES_BATCH_SIZE; sample++) {
						for (i = 0; i < current_number_of_vertices; i++) {
							for (j = 0; j < sizes_in_batch[batch][i]; j++) {
								assert(sample * total_size_batch + starting_points_in_batch[batch][i] + j < total_size_batch * SAMPLES_BATCH_SIZE);
								assert(((sample_batch - 1) * SAMPLES_BATCH_SIZE + sample) * number_of_vertices + S[batch * NODES_BATCH_SIZE + i][j].index < number_of_vertices * number_of_samples);
								if (within_reach_sample_batch[sample * total_size_batch + starting_points_in_batch[batch][i] + j] && !is_infected_by_seeds[((sample_batch - 1) * SAMPLES_BATCH_SIZE + sample) * number_of_vertices + S[batch * NODES_BATCH_SIZE + i][j].index])
									spread[batch * NODES_BATCH_SIZE + i] += 1;
				}	}	}	}
				
				/* transfer data back from the GPU */
				CUDA_CALL(cudaMemcpy(within_reach_sample_batch, dev_within_reach_sample_batch, sizeof(bool) * total_size_batch * SAMPLES_BATCH_SIZE, cudaMemcpyDeviceToHost));
				if (sample_batch == (number_of_sample_batches - 1)) {
					for (sample = 0; sample < SAMPLES_BATCH_SIZE; sample++) {
						for (i = 0; i < current_number_of_vertices; i++) {
							for (j = 0; j < sizes_in_batch[batch][i]; j++) {
								assert(sample * total_size_batch + starting_points_in_batch[batch][i] + j < total_size_batch * SAMPLES_BATCH_SIZE);
								assert((sample_batch * SAMPLES_BATCH_SIZE + sample) * number_of_vertices + S[batch * NODES_BATCH_SIZE + i][j].index < number_of_vertices * number_of_samples);
								if (within_reach_sample_batch[sample * total_size_batch + starting_points_in_batch[batch][i] + j] && !is_infected_by_seeds[(sample_batch * SAMPLES_BATCH_SIZE + sample) * number_of_vertices + S[batch * NODES_BATCH_SIZE + i][j].index])
									spread[batch * NODES_BATCH_SIZE + i] += 1;
			}	}	}	}	}
			
			/* free CPU memory */
			delete[] within_reach_sample_batch;
		}
		cout << "Done computing all batches in " << (double) (clock() - stop) / CLOCKS_PER_SEC << " seconds" << endl;
		stop = clock();
		
		int max_index;
		double max = -(double) INFINITY;
		for (i = 0; i < number_of_vertices; i++) {
			if (!is_seed[i] && spread[i] > max) {
				max = spread[i];
				max_index = i;
		}	}
		
		if (max > -INFINITY) {
			edge_t edge;
			edge.index = max_index;
			//edge.weight = (max / number_of_samples) >= *total_spread ? max / number_of_samples - *total_spread : 0.0;
			edge.weight = max / number_of_samples;
			(*final_seeds).push_back(edge);
			is_seed[max_index] = true;
			cout << "Seed obtained in this round = " << edge.index << ", with marginal spread = " << edge.weight << endl;
			*total_spread += edge.weight;
			for (j = 0; j < S[max_index].size(); j++)
				final_seeds_tree[seed * max_tree_size + j] = S[max_index][j];
		}
		
		stop = clock();
		cout << "Done computing seed " << seed + 1 << " in " << (double) (stop - start) / CLOCKS_PER_SEC << " seconds" << endl;
	}
	
	/* free allocated memory on the CPU */
	delete[] starting_points_in_all_trees;
	delete[] total_size_of_batch;
	delete[] cumulative_size_of_batch;
	delete[] number_of_vertices_in_batch;
	for (batch = 0; batch < number_of_batches; batch++) {
		delete[] starting_points_in_batch[batch];
		delete[] sizes_in_batch[batch];
		delete[] flatten_S_batch[batch];
	}
	delete[] starting_points_in_batch;
	delete[] sizes_in_batch;
	delete[] flatten_S_batch;
	delete[] spread;
	delete[] is_seed;
	delete[] is_infected_by_seeds;
	delete[] final_seeds_tree;
		
	/* free allocated memory on the GPU */
	CUDA_CALL(cudaFree(dev_starting_points));
	CUDA_CALL(cudaFree(dev_sizes));
	CUDA_CALL(cudaFree(dev_flatten_S));
	CUDA_CALL(cudaFree(dev_sample_weights));
	CUDA_CALL(cudaFree(dev_within_reach_sample_batch));
	CUDA_CALL(cudaFree(dev_path_lengths_sample));
	CUDA_CALL(cudaFree(dev_parent_indices));
	CUDA_CALL(cudaFree(dev_is_infected_by_seeds));
	CUDA_CALL(cudaFree(dev_final_seeds_tree));
	CUDA_CALL(cudaFree(dev_sample_weights_for_seeds));
	CUDA_CALL(cudaFree(dev_path_lengths_sample_for_seeds));
	CUDA_CALL(cudaFree(dev_parent_indices_for_seeds));
	
	/* free the random number generator on the GPU */
	CUDA_CALL(cudaFree(dev_state));
	CUDA_CALL(cudaFree(dev_state_for_seeds));
}


/* --- priority queue functions --- */
void update_queue(int index, double *shortest_distance, int *heap_index, int *heap, int *heap_length)
{
	int i, j;

	/* find existing heap entry, or create a new one */
	i = heap_index[index];
	if (!i) 
		i = ++(*heap_length);

	/* upheap */
	for (; i > 1 && shortest_distance[index] < shortest_distance[heap[j = i / 2]]; i = j)
		heap_index[heap[i] = heap[j]] = i;

	heap[i] = index;
	heap_index[index] = i;
}


int pop_queue(double *shortest_distance, int *heap_index, int *heap, int *heap_length)
{
	int i, j;
	int index_to_return, tmp_index;

	if (*heap_length == 0)
		return -1;

	/* remove leading element, pull tail element there and downheap */
	index_to_return = heap[1];
	tmp_index = heap[(*heap_length)--];

	for (i = 1; i < *heap_length && (j = i * 2) <= *heap_length; i = j) {
		if (j < *heap_length && shortest_distance[heap[j]] > shortest_distance[heap[j + 1]]) 
			j++;

		if (shortest_distance[heap[j]] >= shortest_distance[tmp_index]) 
			break;
		heap_index[heap[i] = heap[j]] = i;
	}

	heap[i] = tmp_index;
	heap_index[tmp_index] = i;

	return index_to_return;
}


/* function for computing the shortest path using priority queue Dijkstra algorithm */
void dijkstra_heap(int start_index, int number_of_vertices, int number_of_samples, double sigma, double deadline, double *shortest_distance, double *variance, double *parent_edge_mean, int *parent, int *heap_index, int *heap, int *heap_length)
{
	int i;												/* counter */
	int curr_index;										/* index of current vertex to process */
	int next_index;										/* index of candidate next vertex to process */
	double distance;									/* weight of current edge to process */
	
	parent[start_index] = start_index;
	parent_edge_mean[start_index] = 0.0;
	shortest_distance[start_index] = 0.0;
	variance[start_index] = 0.0;
	update_queue(start_index, shortest_distance, heap_index, heap, heap_length);
	
	while ((curr_index = pop_queue(shortest_distance, heap_index, heap, heap_length)) != -1) {
		if (shortest_distance[curr_index] - sigma * sqrt(variance[curr_index]) >= deadline) {
		//if (shortest_distance[curr_index] * (1 - 1.96 / sqrt(number_of_samples)) >= deadline) {
			break;
		}
		for (i = 0; i < graph[curr_index].size(); i++) {
			next_index = graph[curr_index][i].index;
			distance = shortest_distance[curr_index] + graph[curr_index][i].weight;
			if (distance < shortest_distance[next_index]) {
				parent[next_index] = curr_index;
				parent_edge_mean[next_index] = graph[curr_index][i].weight;
				shortest_distance[next_index] = distance;
				variance[next_index] = variance[curr_index] + pow(graph[curr_index][i].weight, 2);
				update_queue(next_index, shortest_distance, heap_index, heap, heap_length);
	}	}	}
}


/* function to compute the Dijkstra trees within deadline for each node of the graph */
void compute_dijkstra_trees(int number_of_vertices, int number_of_samples, double sigma, double deadline, vector<dijkstra_tree> *S)
{
	int i, j;											/* counters */

	/* variables needed for running Dijkstra */
	double *shortest_distance;							/* array containing the shortest distance of each node from the source node */
	double *variance;									/* array containing the variance of the shortest distance of each node from the source node */
	double *parent_edge_mean;							/* array to store the mean on the edge between the node and its parent in the Dijkstra tree */
	int *parent;										/* array containing the parent index of a node in the shortest path tree */
	int *heap_index;									/* index of the node in the heap */
	int *heap;											/* priority queue or min heap for implementing Dijkstra */
	int heap_length;									/* number of elements in the heap */
	int *reindex_map;									/* array to reindex the Dijkstra tree for each node */
	
	shortest_distance = new (nothrow) double[number_of_vertices];
	variance = new (nothrow) double[number_of_vertices];
	parent_edge_mean = new (nothrow) double[number_of_vertices];
	parent = new (nothrow) int[number_of_vertices];
	heap_index = new (nothrow) int[number_of_vertices];
	heap = new (nothrow) int[number_of_vertices + 1];
	reindex_map = new (nothrow) int[number_of_vertices];
	
	/* for each node, find the shortest distance to all other nodes and estimate its total spread */
	for (i = 0; i < number_of_vertices; i++) {
		heap_length = 0;
		for (j = 0; j < number_of_vertices; j++) {
			shortest_distance[j] = (double) INFINITY;
			variance[j] = - (double) INFINITY;
			parent_edge_mean[j] = -1.0;
			parent[j] = -1;
			heap_index[j] = 0;
			heap[j] = -1;
			reindex_map[j] = -1;
		}
	
		dijkstra_heap(i, number_of_vertices, number_of_samples, sigma, deadline, shortest_distance, variance, parent_edge_mean, parent, heap_index, heap, &heap_length);
		
		dijkstra_tree edge;
		edge.index = i;
		edge.parent_index = i;
		edge.edge_mean = 0.0;
		S[i].push_back(edge);
		
		for (j = 0; j < number_of_vertices; j++) {
			if ((shortest_distance[j] - sigma * sqrt(variance[j]) < deadline) && (j != i)) {
			//if (shortest_distance[j] * (1 - 1.96 / sqrt(number_of_samples)) < deadline) {
				edge.index = j;
				edge.parent_index = parent[j];
				assert(parent_edge_mean[j] > -0.5);
				edge.edge_mean = parent_edge_mean[j];
				S[i].push_back(edge);
		}	}
		
		int reindex = 0;
		for (j = 0; j < S[i].size(); j++)
			reindex_map[S[i][j].index] = reindex++;
		
		for (j = 0; j < S[i].size(); j++) {
			assert(reindex_map[S[i][j].index] != -1);
			S[i][j].reindex = reindex_map[S[i][j].index];
			assert(reindex_map[S[i][j].parent_index] != -1);
			S[i][j].parent_reindex = reindex_map[S[i][j].parent_index];
	}	}
	
	unsigned int sim_total_size = 0, sim_total_count = 0, sim_max_count = 0, max_node;
	for (i = 0; i < number_of_vertices; i++) {
		assert(S[i].size() >= 1);
		sim_total_size += sizeof(dijkstra_tree) * S[i].size();
		sim_total_count += S[i].size();
		if (S[i].size() > sim_max_count) {
			sim_max_count = S[i].size();
			max_node = i;
	}	}
	cout << "Total size of the Dijkstra trees for all nodes (in MB) = " << (double) sim_total_size / 1000000.0 << endl;
	cout << "Total number of entries in all the Dijkstra trees combined = " << sim_total_count << endl;
	cout << "Maximum number of entries in any of the Dijkstra trees = " << sim_max_count << " for node " << max_node << endl;
	
	/* free allocated memory */
	delete[] shortest_distance;
	delete[] variance;
	delete[] parent_edge_mean;
	delete[] parent;
	delete[] heap_index;
	delete[] heap;
	delete[] reindex_map;
}


/* function to prune the graph to a fixed density level */
void prune_graph(int number_of_vertices, int number_of_edges, double density)
{
	if (density > number_of_edges * 1.0 / ((double)number_of_vertices * (number_of_vertices - 1)))
		return;
	
	int i, j, size, index, count = 0;
	int number_of_edges_to_delete = number_of_edges - (int) (density * (double)number_of_vertices * (number_of_vertices - 1));
	bool did_something = false;
	
	while (count < number_of_edges_to_delete) {
		did_something = false;
		for (i = 0; i < number_of_vertices; i++) {
			size = graph[i].size();
			if (size > 1) {
				for (j = 0; j < size; j++) {
					index = graph[i][j].index;
					if (graph[index].size() > 1) {
						graph[i].erase(graph[i].begin() + j);
						count++;
						if (count >= number_of_edges_to_delete)
							return;
						did_something = true;
						break;
		}	}	}	}
		if (!did_something) {
			cout << "Not possible to attain the required density without removing vertices" << endl;
			cout << "Attained density = " << (number_of_edges - count) / ((double)number_of_vertices * (number_of_vertices - 1)) << endl;
			return;
	}	}
}


/* function to read a graph from a file */
void read_graph(char *fileName_graph, int *number_of_vertices, int *number_of_edges)
{
	int i, u, v;
	double alpha, beta;
	ifstream infile;
	
	infile.open(fileName_graph, ios::in);
	if (!infile.is_open()) {
		cout << "Error: File not found." << endl;
		exit(1);
	}
	
	*number_of_vertices = 0;
	*number_of_edges = 0;

	/* parse file and construct adjacency matrix. also extract number of vertices and number of edges. */
	while (infile >> u >> v >> alpha >> beta) {
		if (hash_nodes[u] == 0) {
			(*number_of_vertices)++;
			hash_nodes[u] = 1;
		}
		if (hash_nodes[v] == 0) {
			(*number_of_vertices)++;
			hash_nodes[v] = 1;
		}
		
		edge_t edge;
		edge.index = v;
		edge.weight = alpha * beta;			/* we assume the edge distributions are Gamma(alpha, beta), where alpha is the shape and beta is the scale parameter */
		assert(u < MAX_VERTICES);
		graph[u].push_back(edge);
		(*number_of_edges)++;
	}
	
	long node_total_size = 0, node_total_count = 0, node_max_count = 0, max_node;
	for (i = 0; i < *number_of_vertices; i++) {
		node_total_size += sizeof(edge_t) * graph[i].size();
		node_total_count += graph[i].size();
		if (graph[i].size() > node_max_count) {
			node_max_count = graph[i].size();
			max_node = i;
	}	}
	if (node_total_count != *number_of_edges)
		cout << "Warning: Node total count = " << node_total_count << ", number of edges = " << *number_of_edges << endl;
	cout << "Total number of vertices = " << *number_of_vertices << endl;
	cout << "Total density of the graph = " << *number_of_edges / ((double)(*number_of_vertices) * (*number_of_vertices - 1)) << endl;
	cout << "Total size of the graph adjacency list (in MB) = " << (double) node_total_size / 1000000.0 << endl;
	cout << "Total number of entries in the graph adjacency list = " << node_total_count << endl;
	cout << "Maximum length of any row in the graph adjacency list = " << node_max_count << " for node " << max_node << endl;
	
	infile.close();
}


/* function to print the adjacency list of the graph */
void print_graph(int number_of_vertices)
{
	int i, j;
	cout << "The following is the adjacency list of the graph:" << endl;
	for (i = 0; i < number_of_vertices; i++) {
		cout << setw(10) << i;
		for(j = 0; j < graph[i].size(); j++)
			cout << setw(10) << graph[i][j].index << " (" << graph[i][j].weight << ") ";
		cout << endl;		
	}
	cout << endl;
}


/* function to print the similarity matrix */
void print_similarity(int number_of_vertices, vector<dijkstra_tree> *S)
{
	int i, j;
	cout << "The following are the dijkstra trees for all nodes of the graph:" << endl;
	for (i = 0; i < number_of_vertices; i++) {
		if (S[i].size() > 0) {
			cout << setw(10) << i << endl;
			for (j = 0; j < S[i].size(); j++)
				cout << setw(10) << S[i][j].index << setw(10) << S[i][j].reindex << setw(10) << S[i][j].parent_index 
						<< setw(10) << S[i][j].parent_reindex << setw(10) << S[i][j].edge_mean << endl;
	}	}
	cout << endl;
}


/* function to print the seeds and their spreads */
void print_seeds(vector<edge_t> seeds)
{
	int i;
	cout << "\nThe following is the list of seeds with their marginal spread:" << endl;
	for (i = 0; i < seeds.size(); i++)
		cout << "Seed = " << seeds[i].index << ", Spread = " << seeds[i].weight << endl;
}



int main(int argc, char *argv[])
{
	double time = 0;							/* time counter */
	int number_of_samples;						/* number of times simulation is run */
	int number_of_seeds;						/* number of seeds to return */
	int number_of_vertices, number_of_edges;	/* graph parameters */
	double density;								/* graph parameter */
	double deadline = 0.1;						/* deadline */
	double extended_deadline = deadline * 1;	/* extended deadline for getting paths */
	double sigma = 0.3;							/* parameter for considering deviation around mean */
	clock_t start, stop;						/* variables for measuring time taken by the CPU */
	
	char *fileName = argv[1];					/* input file path passed as an argument in console */
	number_of_samples = atoi(argv[2]);			/* no. of iterations passed as argument in console */
	number_of_seeds = atoi(argv[3]);			/* no. of seeds to return passed as an argument in console */
	density = atof(argv[4]);					/* density of graph to process passed as an argument in console */
	
	cout << "Reading graph from file" << endl;
	start = clock();
	rand_val(start);
	read_graph(fileName, &number_of_vertices, &number_of_edges);
	//print_graph(number_of_vertices);
	stop = clock();
	cout << "Done reading graph from file in " << (double) (stop - start) / CLOCKS_PER_SEC << " seconds" << endl;
	time += (double) (stop - start) / CLOCKS_PER_SEC;
	
	if (density < number_of_edges * 1.0 / ((double)number_of_vertices * (number_of_vertices - 1))) {
		cout << "\nPruning graph to the required density" << endl;
		start = clock();
		rand_val(start);
		prune_graph(number_of_vertices, number_of_edges, density);
		//print_graph(number_of_vertices);
		stop = clock();
		cout << "Done pruning graph in " << (double) (stop - start) / CLOCKS_PER_SEC << " seconds" << endl;
		time += (double) (stop - start) / CLOCKS_PER_SEC;
	}
	
	/* compute the Dijkstra tree for each node by finding the shortest paths to other nodes within the deadline */
	cout << "\nComputing shortest paths with deadline = " << extended_deadline << " and sigma = " << sigma << endl;
	vector<dijkstra_tree> *S = new (nothrow) vector<dijkstra_tree>[number_of_vertices];
	if (S == NULL) {
		cout << "Error: Memory could not be allocated." << endl;
		exit(1);
	}
	start = clock();
	compute_dijkstra_trees(number_of_vertices, number_of_samples, sigma, extended_deadline, S);
	//print_similarity(number_of_vertices, S);
	stop = clock();
	cout << "Done computing shortest paths in " << (double) (stop - start) / CLOCKS_PER_SEC << " seconds" << endl;
	time += (stop - start) / CLOCKS_PER_SEC;
	
	/* compute the seeds */
	number_of_seeds = number_of_seeds < number_of_vertices? number_of_seeds: (number_of_vertices - 1);
	cout << "\nComputing " << number_of_seeds << " seeds" << endl;
	double total_spread = 0.0;
	vector<edge_t> final_seeds;					/* collect the seeds */
	start = clock();
	compute_seeds(number_of_vertices, number_of_seeds, number_of_samples, deadline, sigma, &total_spread, &final_seeds, S);
	stop = clock();
	cout << "\nDone computing " << number_of_seeds << " seeds in " << (double) (stop - start) / CLOCKS_PER_SEC << " seconds" << endl;
	time += (double) (stop - start) / CLOCKS_PER_SEC;
	
	/* compute the expected spread of each node */
	cout << "\nFinal Results:" << endl;
	print_seeds(final_seeds);
	cout << "Total spread = " << total_spread << endl;
	cout << "\nTotal time taken = " << time << " seconds" << endl;
	
	/* free allocated memory */
	delete[] S;
	
	return 0;
}