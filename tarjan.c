/****************************************
	Authors: Koushik Pal
			 Zissis Poulos
****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <time.h>


#define MAX_VERTICES 80000
#define MAX_OUTDEGREE 5000


typedef struct node_t node_t;

struct node_t {
	long ID;											/* unique ID of the node */
	int index;											/* location of the node in the global nodes array */
	int in_degree;										/* in-degree of the node in the graph */
	int out_degree;										/* out-degree of the node in the graph */
	int outgoing_edges[MAX_OUTDEGREE];					/* list of edges going out from the node */
};

node_t nodes[MAX_VERTICES] = {0};
double weights[MAX_VERTICES][MAX_OUTDEGREE] = {0};


typedef struct bridge_t bridge_t;

struct bridge_t {
	int vertex1;
	int vertex2;
	int component1;
	int component2;
};


/* comparator function for qsort for sorting by first component and within that sorting by second component */
int compare_by_component(const void *a, const void *b)
{
	bridge_t *ia = (bridge_t *)a;
	bridge_t *ib = (bridge_t *)b;
	
	if (ia->component1 == ib->component1)
		return (ia->component2 - ib->component2);
	return (ia->component1 - ib->component1); 
}


/* function to create a bridge_t object from given values */
void create_bridge(bridge_t *d, int v1, int v2, int c1, int c2)
{
	d->vertex1 = v1;
	d->vertex2 = v2;
	d->component1 = c1;
	d->component2 = c2;
}


/* function to construct the array of all edges going between components */
void identify_bridges(int number_of_vertices, int *number_of_bridges, int *scc, bridge_t *bridges)
{
	int i, index_v, index_w, c1, c2;
	for (index_v = 0; index_v < number_of_vertices; index_v++) {
		c1 = scc[index_v];
		for (i = 0; i < nodes[index_v].out_degree; i++) {
			index_w = nodes[index_v].outgoing_edges[i];
			c2 = scc[index_w];
			if (c1 != c2)
				create_bridge(&bridges[(*number_of_bridges)++], index_v, index_w, c1, c2);
		}
	}
}


/* function implementing Tarjan's recursive strongly connected components detection algorithm */
void scc_recursive(int index_v, int *number_of_components, int *max_component_size, int *tos, int *n_visited, int *nodes_to_components, int * component_sizes, int *stack, int *on_stack, int *visited_index, int *low_link_index)
{
    int i, vpos, index_w, count;

    /* initialize visited_index and low_link_index of vertex v, and increase the counter for the number of visited vertices */
    visited_index[index_v] = low_link_index[index_v] = *n_visited;
    (*n_visited)++;

    /* place v on the stack */
    stack[*tos] = index_v;
    on_stack[index_v] = 1;
    (*tos)++;
    
    /* note that the algorithm is like a recursive DFS from each w in OUT(v) that is still unvisited */
	for (i = 0; i < nodes[index_v].out_degree; i++) {
		index_w = nodes[index_v].outgoing_edges[i];
		if (visited_index[index_w] == -1) {
			scc_recursive(index_w, number_of_components, max_component_size, tos, n_visited, nodes_to_components, component_sizes, stack, on_stack, visited_index, low_link_index);
			/* update low_link index */
			if (low_link_index[index_w] < low_link_index[index_v])
				low_link_index[index_v] = low_link_index[index_w];
		}
		else if (on_stack[index_w]) {
			if (visited_index[index_w] < low_link_index[index_v])
				low_link_index[index_v] = visited_index[index_w];
		}
	}
	
    /* create a new component if all vertices in v's strongly connected component have been found */
    if (low_link_index[index_v] == visited_index[index_v]) {
    	(*number_of_components)++;
    	count = 0;
    	do {
    		vpos = stack[*tos - 1];
    		nodes_to_components[vpos] = *number_of_components;
    		on_stack[vpos] = 0;
    		(*tos)--;
    		count++;
    	} while (vpos != index_v);
    	component_sizes[*number_of_components] = count;
    }
}


/* function calling Tarjan's algorithm from all unvisited nodes */
void tarjan(int number_of_vertices, int *number_of_components, int *nodes_to_components, int *component_sizes)
{
    int i, tos = 0, n_visited = 0, max_component_size = 0;
    
    int *stack, *on_stack, *visited_index, *low_link_index;

    /* allocate space for arrays required for running Tarjan's algorithm */
    stack = (int *)malloc(sizeof(int) * number_of_vertices);
    on_stack = (int *)malloc(sizeof(int) * number_of_vertices);
    visited_index = (int *)malloc(sizeof(int) * number_of_vertices);
    low_link_index = (int *)malloc(sizeof(int) * number_of_vertices);
    
    /* initialize visited_index[] and on_stack[] array entries to unvisited and false respectively */
    for (i = 0; i < number_of_vertices; i++) {
		visited_index[i] = -1;
		on_stack[i] = 0;
	}
    
    /* Tarjan's algorithm proceeds as a recursive depth first search.  Note
     * that the scc_recursive() function accesses the current graph through the
     * global variable `nodes'.  If parts of the graph were not visited,
     * scc_recursive() will be called again, until all vertices have been visited.
     */
    for (i = 0; i < number_of_vertices; i++) {
        if (visited_index[i] == -1)
        	scc_recursive(i, number_of_components, &max_component_size, &tos, &n_visited, nodes_to_components, component_sizes, stack, on_stack, visited_index, low_link_index);
    }

    /* free allocated space */
    free(stack);
    free(on_stack);
    free(visited_index);
    free(low_link_index);
}


/* function to read a graph from a file */
void read_graph(char *fileName, int *number_of_vertices, int *number_of_edges)
{
	FILE *fin = fopen(fileName, "r");
	
	/* first row of the file is number of edges, lambda and the deadline */
 	fscanf(fin, "%d", number_of_edges);

	int num_vertices = 0;
	int i, j, k;
	long u, v;
	double w;

	/* parse file and construct adjacency matrix adj_matrix[][]. also extract number of vertices V and unique IDs */
	srand(11);
	for (i = 0; i < *number_of_edges; i++) {
		j = 0;
		k = 0;
		
		//fscanf(fin, "%ld%ld%lf", &u, &v, &w);
		fscanf(fin, "%ld%ld", &u, &v);
		w = (rand() + 1.0) / (RAND_MAX + 2.0);
		
		while (j < num_vertices && u != nodes[j].ID)
			j++;
		
		if (j < num_vertices)
			nodes[j].in_degree++;
		else {
			nodes[j].ID = u;
			nodes[j].index = j;
			nodes[j].in_degree = 1;
			nodes[j].out_degree = 0;
			num_vertices = j + 1;
		}
		
		while (k < num_vertices && v != nodes[k].ID)
			k++;
		
		if (k < num_vertices) {
			nodes[k].out_degree++;
			assert(nodes[k].out_degree < MAX_OUTDEGREE);
		}
		else {
			nodes[k].ID = v;
			nodes[k].index = k;
			nodes[k].in_degree = 0;
			nodes[k].out_degree = 1;
			num_vertices = k + 1;
		}
		
		nodes[k].outgoing_edges[nodes[k].out_degree - 1] = j;
		weights[k][j] = -log(w);
	}
	
	fclose(fin);
	
	*number_of_vertices = num_vertices;
	assert(num_vertices < MAX_VERTICES);
}


/* function to print the adjacency list of the graph */
void print_graph(int number_of_vertices)
{
	int i, j;
	printf("The following is the adjacency list of the graph:\n");
	for (i = 0; i < number_of_vertices; i++) {
		printf("%10ld ", nodes[i].ID);
		for(j = 0; j < nodes[i].out_degree; j++)
			printf("%10ld ", nodes[nodes[i].outgoing_edges[j]].ID);
		printf("\n");		
	}
	printf("\n");
}


/* function to print the adjacency matrix of weights */
void print_weights(int number_of_vertices)
{
	int i, j;
	printf("The following is the adjacency matrix of the weights:\n");
	for (i = 0; i < number_of_vertices; i++) {
		for(j = 0; j < number_of_vertices; j++)
			printf("%10lf ", weights[i][j]);
		printf("\n");		
	}
	printf("\n");
}


/* function to print the components sizes */
void print_component_sizes(int number_of_components, int *component_sizes)
{
	int i;
	printf("\nSizes of strongly connected components with size greater than one:\n");
	for (i = 0; i <= number_of_components; i++)
		if (component_sizes[i] > 1)
			printf("Size of Component %d = %d\n", i + 1, component_sizes[i]);
	printf("\n");
}


/* function to print the components array */
void print_component_assignments(int number_of_vertices, int *nodes_to_components)
{
	int i;
	printf("Nodes -> Components Assignment:\n");
	for (i = 0; i < number_of_vertices; i++)
		printf("%10ld -> %10d\n", nodes[i].ID, nodes_to_components[i] + 1);
	printf("\n");
}


/* function to print the bridges */
void print_bridges(int number_of_bridges, bridge_t *bridges)
{
	int i;
	printf("List of bridge edges:\n");
	for (i = 0; i < number_of_bridges; i++)
		printf("%10ld %10ld %10d %10d\n", nodes[bridges[i].vertex1].ID, nodes[bridges[i].vertex2].ID, bridges[i].component1 + 1, bridges[i].component2 + 1);
	printf("\n");
}



int main(int argc, char *argv[])
{
	int i, j;											/* counters */
	double time = 0;									/* time counter */
	int number_of_vertices, number_of_edges;			/* graph parameters */
	int number_of_iterations;							/* number of iterations the program runs */
	double lambda = 0.8;								/* damping factor */
	double deadline = (double) INFINITY;				/* deadline */
	clock_t start, stop;								/* variables for measuring time taken by the CPU */
	
	char *fileName = argv[1];							/* input file path passed as an argument in console */
	number_of_iterations = atoi(argv[2]);				/* no. of iterations passed as argument in console */

	printf("Reading graph from file\n");
	start = clock();
	read_graph(fileName, &number_of_vertices, &number_of_edges);
	stop = clock();
	printf("Done reading graph from file in %lf seconds\n", (double) (stop - start)/CLOCKS_PER_SEC);
	time += (double) (stop - start)/CLOCKS_PER_SEC;
	printf("\nNumber of vertices = %d\n", number_of_vertices);
	printf("Number of edges = %d\n", number_of_edges);
	
	//print_graph(number_of_vertices);
	//print_weights(number_of_vertices);
	
	int number_of_components = -1;
	int *nodes_to_components, *component_sizes;
	nodes_to_components = (int *)malloc(sizeof(int) * number_of_vertices);
	component_sizes = (int *)malloc(sizeof(int) * number_of_vertices);
	tarjan(number_of_vertices, &number_of_components, nodes_to_components, component_sizes);
	printf("\nNumber of strongly connected components = %d\n", number_of_components + 1);
	print_component_sizes(number_of_components, component_sizes);
	//print_component_assignments(number_of_vertices, nodes_to_components);
	
	int number_of_bridges = 0;
	bridge_t *bridges;
	bridges = (bridge_t *)malloc(sizeof(bridge_t) * number_of_edges);
	identify_bridges(number_of_vertices, &number_of_bridges, nodes_to_components, bridges);
	qsort(bridges, number_of_bridges, sizeof(bridge_t), compare_by_component);
	printf("Number of bridges edges = %d\n", number_of_bridges);
	//print_bridges(number_of_bridges, bridges);
	
	free(nodes_to_components);
	free(component_sizes);
	free(bridges);
	
	return 0;
}
