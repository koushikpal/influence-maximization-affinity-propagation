/****************************************
	Authors: Koushik Pal
			 Zissis Poulos
****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <time.h>

#define CUDA_CALL(x)\
{\
    const cudaError_t a = (x);\
    if (a != cudaSuccess) {\
        printf("\nCUDA Error: %s (err_num=%d) \n", cudaGetErrorString(a), a);\
        cudaDeviceReset();\
        assert(0);\
    }\
}

#define MAX_SIZE 1600
#define MAX_VERTICES 1300
#define MAX_OUTDEGREE 1300
#define numberOfThreadsPerBlock 512


typedef struct node_t node_t;
typedef struct community_t community_t;
typedef struct spread_t spread_t;

struct node_t {
	int ID;												/* unique ID of the node */
	int index;											/* location of the node in the global nodes array */
	int in_degree;										/* in-degree of the node in the graph */
	int out_degree;										/* out-degree of the node in the graph */
	int outgoing_edges[MAX_OUTDEGREE];					/* list of edges going out from the node */
};

struct community_t {
	int vertices[MAX_SIZE];
	int size;
};

struct spread_t {
	int index;
	int spread;
};


/* comparator function for using qsort to sort exemplars by their spread */
int compare_by_spread (const void * a, const void * b)
{
	spread_t *spread_a = (spread_t *) a;
	spread_t *spread_b = (spread_t *) b;
	return (spread_b->spread - spread_a->spread);
}


/* --- priority queue functions --- */
__device__ void insert_queue(int index, double distance, double *shortest_distance, int *visited, int *heap_index, int *heap, int *heap_length)
{
	int i, j;

	/* already knew better path */
	if (visited[index] == 1 && distance >= shortest_distance[index])
		return;

	/* find existing heap entry, or create a new one */
	visited[index] = 1;
	shortest_distance[index] = distance;

	i = heap_index[index];
	if (!i) 
		i = ++(*heap_length);

	/* upheap */
	for (; i > 1 && shortest_distance[index] < shortest_distance[heap[j = i/2]]; i = j)
		heap_index[heap[i] = heap[j]] = i;

	heap[i] = index;
	heap_index[index] = i;
}


__device__ int pop_queue(double *shortest_distance, int *heap_index, int *heap, int *heap_length)
{
	int i, j;
	int index_to_return, tmp_index;

	if (*heap_length == 0)
		return -1;

	/* remove leading element, pull tail element there and downheap */
	index_to_return = heap[1];
	tmp_index = heap[(*heap_length)--];

	for (i = 1; i < *heap_length && (j = i * 2) <= *heap_length; i = j) {
		if (j < *heap_length && shortest_distance[heap[j]] > shortest_distance[heap[j + 1]]) 
			j++;

		if (shortest_distance[heap[j]] >= shortest_distance[tmp_index]) 
			break;
		heap_index[heap[i] = heap[j]] = i;
	}

	heap[i] = tmp_index;
	heap_index[tmp_index] = i;

	return index_to_return;
}


/* function for computing the shortest path using priority queue dijkstra algorithm */
__device__ void dijkstra_heap(int start_index, int number_of_vertices, double *shortest_distance, int *visited, int *heap_index, int *heap, int *heap_length, node_t *nodes, double *weights)
{
	int i;										/* counter */
	int curr_index;								/* index of current vertex to process */
	int next_index;								/* index of candidate next vertex to process */

	insert_queue(start_index, 0.0, shortest_distance, visited, heap_index, heap, heap_length);
	while ((curr_index = pop_queue(shortest_distance, heap_index, heap, heap_length)) != -1) {
		for (i = 0; i < nodes[curr_index].out_degree; i++) {
			next_index = nodes[curr_index].outgoing_edges[i];
			insert_queue(next_index, shortest_distance[curr_index] + weights[curr_index * MAX_OUTDEGREE + next_index], shortest_distance, visited, heap_index, heap, heap_length);
		}
	}
}


/* function for computing the shortest path using dynamic dijkstra algorithm */
__device__ void dijkstra_dynamic(int start_index, int number_of_vertices, double *shortest_distance, int *visited, node_t *nodes, double *weights)
{
	int i;										/* counter */
	int index_v;								/* index of current vertex to process */
	int index_w;								/* index of candidate next vertex */
	double min_distance;						/* best current distance from start */
	double weight;								/* weight of current edge to process */

	index_v = start_index;
	shortest_distance[index_v] = 0;

	while (visited[index_v] == 0) {
		visited[index_v] = 1;
		for (i = 0; i < nodes[index_v].out_degree; i++) {
			index_w = nodes[index_v].outgoing_edges[i];
			weight = weights[index_v * MAX_OUTDEGREE + index_w];
			
			if (shortest_distance[index_w] > (shortest_distance[index_v] + weight))
				shortest_distance[index_w] = shortest_distance[index_v] + weight;
		}

		index_v = 0;
		min_distance = (double) INFINITY;
		for (i = 0; i < number_of_vertices; i++) {
			if ((visited[i] == 0) && (min_distance > shortest_distance[i])) {
				min_distance = shortest_distance[i];
				index_v = i;
			}
		}
	}
}


/* function for computing the spread of each exemplar */
void compute_spread(int number_of_vertices, int *assignments, node_t *nodes, int *spread)
{
	int i;
	
	for (i = 0; i < number_of_vertices; i++)
		spread[nodes[assignments[i]].ID]++;
}


/* function for finding the exemplars */
__global__ void compute_exemplars(int number_of_vertices, double *dev_R, double *dev_A, int *dev_exemplars, int *dev_number_of_exemplars)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	int i;
	double eval;
	
	if (tid == 0) {
		*dev_number_of_exemplars = 0;
		for (i = 0; i < number_of_vertices; i++) {
			eval = dev_R[i * number_of_vertices + i] + dev_A[i * number_of_vertices + i];
			if (eval > 0.0)
				dev_exemplars[(*dev_number_of_exemplars)++] = i;
		}
	}
}


/* function for assigning an exemplar to each node */
__global__ void compute_assignments(int number_of_vertices, int *dev_number_of_exemplars, int *dev_exemplars, double *dev_S, int *dev_assignments)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	int idxForI, j, c;
	double max;
	
	while (tid < number_of_vertices) {
		idxForI = 0;
		max = -INFINITY;
		for (j = 0; j < *dev_number_of_exemplars; j++) {
			c = dev_exemplars[j];
			if (dev_S[tid * number_of_vertices + c] > max) {
				max = dev_S[tid * number_of_vertices + c];
				idxForI = c;
			}
		}
		dev_assignments[tid] = idxForI;
		
		tid += blockDim.x * gridDim.x;
	}
}


/* function for computing and updating responsibility */
__global__ void compute_responsibility(int iteration, int number_of_vertices, double lambda, double *dev_S, double *dev_R, double *dev_A)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	int j, k, max_index, second_max_index;
	double max, second_max;
	
	while (tid < number_of_vertices) {
		if (iteration == 0) {
			for (k = 0; k < number_of_vertices * number_of_vertices; k++)
				dev_A[k] = 0.0;
		}
	
		if (dev_S[tid * number_of_vertices + 0] + dev_A[tid * number_of_vertices + 0] < dev_S[tid * number_of_vertices + 1] + dev_A[tid * number_of_vertices + 1]) {
			max = dev_S[tid * number_of_vertices + 1] + dev_A[tid * number_of_vertices + 1];
			max_index = 1;
			second_max = dev_S[tid * number_of_vertices + 0] + dev_A[tid * number_of_vertices + 0];
			second_max_index = 0;
		}
		else {
			max = dev_S[tid * number_of_vertices + 0] + dev_A[tid * number_of_vertices + 0];
			max_index = 0;
			second_max = dev_S[tid * number_of_vertices + 1] + dev_A[tid * number_of_vertices + 1];
			second_max_index = 1;
		}
		for (k = 2; k < number_of_vertices; k++) {
			if (dev_S[tid * number_of_vertices + k] + dev_A[tid * number_of_vertices + k] >= max) {
				second_max = max;
				second_max_index = max_index;
				max = dev_S[tid * number_of_vertices + k] + dev_A[tid * number_of_vertices + k];
				max_index = k;
			}
		}
		assert(max_index != second_max_index);
			
		for (j = 0; j < number_of_vertices; j++) {
			if (j != max_index) {
				if (fabs(lambda) > 1E-10)
					dev_R[tid * number_of_vertices + j] = (1 - lambda) * (dev_S[tid * number_of_vertices + j] - max) + lambda * dev_R[tid * number_of_vertices + j];
				else
					dev_R[tid * number_of_vertices + j] = dev_S[tid * number_of_vertices + j] - max;
			}
			else {
				if (fabs(lambda) > 1E-10)
					dev_R[tid * number_of_vertices + j] = (1 - lambda) * (dev_S[tid * number_of_vertices + j] - second_max) + lambda * dev_R[tid * number_of_vertices + j];
				else
					dev_R[tid * number_of_vertices + j] = dev_S[tid * number_of_vertices + j] - second_max;
			}
		}
		
		tid += blockDim.x * gridDim.x;
	}
}


/* function for computing the sum matrix needed for updating availability */
__global__ void compute_sum_matrix(int number_of_vertices, double *dev_R, double *dev_sum_matrix)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	int k;
	
	while (tid < number_of_vertices) {
		dev_sum_matrix[tid] = 0.0;
		for (k = 0; k < number_of_vertices; k++) {
			if (k != tid)
				dev_sum_matrix[tid] += fmax(0.0, dev_R[k * number_of_vertices + tid]);
		}
		
		tid += blockDim.x * gridDim.x;
	}
}


/* function for computing and updating availability */
__global__ void compute_availability(int number_of_vertices, double lambda, double *dev_R, double *dev_A, double *dev_sum_matrix)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	int j;
	double sum;
	
	while (tid < number_of_vertices) {
		for (j = 0; j < number_of_vertices; j++) {
			if (tid == j) {
				sum = dev_sum_matrix[j];
				if (fabs(lambda) > 1E-10)
					dev_A[tid * number_of_vertices + j] = (1 - lambda) * sum + lambda * dev_A[tid * number_of_vertices + j];
				else
					dev_A[tid * number_of_vertices + j] = sum;
			} else {
				sum = dev_sum_matrix[j] - fmax(0.0, dev_R[tid * number_of_vertices + j]);
				if (fabs(lambda) > 1E-10)
					dev_A[tid * number_of_vertices + j] = (1 - lambda) * fmin(0.0, dev_R[j * number_of_vertices + j] + sum) + lambda * dev_A[tid * number_of_vertices + j];
				else
					dev_A[tid * number_of_vertices + j] = fmin(0.0, dev_R[j * number_of_vertices + j] + sum);
			}
		}
	
		tid += blockDim.x * gridDim.x;
	}
}


/* function for computing the similarity matrix */
__global__ void compute_similarity(int number_of_vertices, int number_of_edges, double deadline, double *dev_S, node_t *dev_nodes, double *dev_weights)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	int i;													/* counter */

	while (tid < number_of_vertices) {
		/* variables needed for running dijkstra */
		double *shortest_distance;							/* array containing the shortest distance of each node from the source node */
		int *visited;										/* array containing 1s and 0s for nodes that are in the shortest path tree or out */
		int *heap_index;									/* index of the node in the heap */
		int *heap;											/* priority queue or min heap for implementing dijkstra */
		int heap_length;									/* number of elements in the heap */
		
		shortest_distance = (double *) malloc(sizeof(double) * number_of_vertices);
		visited = (int *) malloc(sizeof(int) * number_of_vertices);
		heap_index = (int *) malloc(sizeof(int) * number_of_vertices);
		heap = (int *) malloc(sizeof(int) * (number_of_vertices + 1));
	
		/* for each node, find the shortest distance to all other nodes and estimate its total spread */
		heap_length = 0;
		for (i = 0; i < number_of_vertices; i++) {
			shortest_distance[i] = (double) INFINITY;
			visited[i] = 0;
			heap_index[i] = 0;
			heap[i] = -1;
		}
		
		dijkstra_heap(tid, number_of_vertices, shortest_distance, visited, heap_index, heap, &heap_length, dev_nodes, dev_weights);
		//dijkstra_dynamic(tid, number_of_vertices, shortest_distance, visited, dev_nodes, dev_weights);
		
		for (i = 0; i < number_of_vertices; i++) {
			if (shortest_distance[i] < deadline)
				dev_S[tid * number_of_vertices + i] = -shortest_distance[i];
			else
				dev_S[tid * number_of_vertices + i] = -INFINITY;
		}
		
		/* free allocated memory */
		free(shortest_distance);
		free(visited);
		free(heap_index);
		free(heap);
		
		tid += blockDim.x * gridDim.x;
	}
}


/* function for computing the self similarities */
__global__ void compute_self_similarity(int number_of_vertices, double *dev_S)
{
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	int i, j, count = 0;
	double sum = 0.0;
	
	if (tid == 0) {
		for (i = 0; i < number_of_vertices; i++) {
			for (j = 0; j < number_of_vertices; j++) {
				if (dev_S[i * number_of_vertices + j] > -INFINITY) {
					sum += dev_S[i * number_of_vertices + j];
					count++;
				}
			}
		}
	
		if (sum >= 0.0)
			sum = -INFINITY;
	
		for (i = 0; i < number_of_vertices; i++)
			dev_S[i * number_of_vertices + i] = sum;
	}
}


void community_spread_CPU(int number_of_iterations, int number_of_vertices, int number_of_edges, double lambda, double deadline, int *spread, node_t *nodes, double *weights)
{
	printf("Too big to fit on the GPU\n");
}


/* function for setting up the GPU parameters for computing the similarity matrix and running affinity propagation */
void community_spread_GPU(int number_of_iterations, int number_of_vertices, int number_of_edges, double lambda, double deadline, int *spread, node_t *nodes, double *weights)
{
	int numberOfBlocks = (number_of_vertices + numberOfThreadsPerBlock - 1)/numberOfThreadsPerBlock;
	
	/* compute similarity */
	
	node_t *dev_nodes;
	double *dev_weights, *dev_S;
	CUDA_CALL(cudaMalloc((void**) &dev_nodes, sizeof(node_t) * number_of_vertices));
	CUDA_CALL(cudaMalloc((void**) &dev_weights, sizeof(double) * number_of_vertices * MAX_OUTDEGREE));
	CUDA_CALL(cudaMalloc((void**) &dev_S, sizeof(double) * number_of_vertices * number_of_vertices));
	
	CUDA_CALL(cudaMemcpy(dev_nodes, nodes, sizeof(node_t) * number_of_vertices, cudaMemcpyHostToDevice));
	CUDA_CALL(cudaMemcpy(dev_weights, weights, sizeof(double) * number_of_vertices * MAX_OUTDEGREE, cudaMemcpyHostToDevice));
	
	compute_similarity<<<numberOfBlocks, numberOfThreadsPerBlock>>>(number_of_vertices, number_of_edges, deadline, dev_S, dev_nodes, dev_weights);
	CUDA_CALL(cudaDeviceSynchronize());
	compute_self_similarity<<<1, 1>>>(number_of_vertices, dev_S);
	CUDA_CALL(cudaDeviceSynchronize());
	
	/* run affinity propagation */
	
	double *dev_R, *dev_A, *dev_sum_matrix;
	CUDA_CALL(cudaMalloc((void**) &dev_R, sizeof(double) * number_of_vertices * number_of_vertices));
	CUDA_CALL(cudaMalloc((void**) &dev_A, sizeof(double) * number_of_vertices * number_of_vertices));
	CUDA_CALL(cudaMalloc((void**) &dev_sum_matrix, sizeof(double) * number_of_vertices));
	
	double *A = (double *) malloc(sizeof(double) * number_of_vertices * number_of_vertices);
	double *R = (double *) malloc(sizeof(double) * number_of_vertices * number_of_vertices);
	
	for (int m = 0; m < number_of_iterations; m++) {
		
		compute_responsibility<<<numberOfBlocks, numberOfThreadsPerBlock>>>(m, number_of_vertices, lambda, dev_S, dev_R, dev_A);
		CUDA_CALL(cudaDeviceSynchronize());
		compute_sum_matrix<<<numberOfBlocks, numberOfThreadsPerBlock>>>(number_of_vertices, dev_R, dev_sum_matrix);
		CUDA_CALL(cudaDeviceSynchronize());
		compute_availability<<<numberOfBlocks, numberOfThreadsPerBlock>>>(number_of_vertices, lambda, dev_R, dev_A, dev_sum_matrix);
		CUDA_CALL(cudaDeviceSynchronize());
		
	}
	
	/* find exemplars and compute assignments */
	
	int *assignments, *dev_number_of_exemplars, *dev_exemplars, *dev_assignments;
	assignments = (int *)malloc(sizeof(int) * number_of_vertices);
	CUDA_CALL(cudaMalloc((void**) &dev_number_of_exemplars, sizeof(int) * 1));
	CUDA_CALL(cudaMalloc((void**) &dev_exemplars, sizeof(int) * number_of_vertices));
	CUDA_CALL(cudaMalloc((void**) &dev_assignments, sizeof(int) * number_of_vertices));
	
	compute_exemplars<<<1, 1>>>(number_of_vertices, dev_R, dev_A, dev_exemplars, dev_number_of_exemplars);
	CUDA_CALL(cudaDeviceSynchronize());
	
	compute_assignments<<<numberOfBlocks, numberOfThreadsPerBlock>>>(number_of_vertices, dev_number_of_exemplars, dev_exemplars, dev_S, dev_assignments);
	CUDA_CALL(cudaMemcpy(assignments, dev_assignments, sizeof(int) * number_of_vertices, cudaMemcpyDeviceToHost));
	
	/* compute spread of each exemplar */
	
	compute_spread(number_of_vertices, assignments, nodes, spread);
	
	/* free allocated GPU memory */
	cudaFree(dev_nodes);
	cudaFree(dev_weights);
	cudaFree(dev_S);
	cudaFree(dev_R);
	cudaFree(dev_A);
	cudaFree(dev_sum_matrix);
	cudaFree(dev_number_of_exemplars);
	cudaFree(dev_exemplars);
	cudaFree(dev_assignments);
	
	/* free allocated CPU memory */
	free(assignments);
}


/* function to compute the induced graph corresponding to a community */
void compute_induced_graph(char *fileName_graph, int community_index, int number_of_vertices, int number_of_induced_vertices, int *number_of_induced_edges, community_t *communities, int *nodes2community, node_t *induced_nodes, double *induced_weights)
{
	int k, l, u, v, index = 0;
	double w;
	
	/* re-index the nodes of the big graph to fit the small graph */
	int *node_index_in_community = (int *) malloc(sizeof(int) * number_of_vertices);
	for (v = 0; v < number_of_vertices; v++) {
		if (nodes2community[v] == community_index) {
			assert(index < number_of_induced_vertices);
			induced_nodes[index].ID = v;
			induced_nodes[index].index = index;
			induced_nodes[index].in_degree = 0;
			induced_nodes[index].out_degree = 0;
			node_index_in_community[v] = index++;
		}
		else
			node_index_in_community[v] = -1;
	}
	
	assert(index == number_of_induced_vertices);
	
	FILE *fin = fopen(fileName_graph, "r");
	
	while (fscanf(fin, "%d%d%lf", &u, &v, &w) == 3) {
		l = node_index_in_community[u];
		k = node_index_in_community[v];
		if (k != -1 && l != -1) {
			induced_nodes[l].in_degree++;
			induced_nodes[k].outgoing_edges[induced_nodes[k].out_degree++] = l;
			assert(induced_nodes[k].out_degree < MAX_OUTDEGREE);
			induced_weights[k * MAX_OUTDEGREE + l] = -log(w);
			(*number_of_induced_edges)++;
		}
	}
	
	fclose(fin);
	free(node_index_in_community);
}


/* function to read the number of communities from a .map file */
void read_number_of_communities(char *fileName_map, int *number_of_vertices, int *number_of_edges, int *number_of_communities)
{
	char c, s[20];
	int num;
	
	FILE *fin = fopen(fileName_map, "r");
	fscanf(fin, "%c%s%d\n", &c, s, number_of_communities);
	fscanf(fin, "%c%s%d\n", &c, s, &num);
	fscanf(fin, "%c%s%d\n", &c, s, number_of_vertices);
	fscanf(fin, "%c%s%d\n", &c, s, number_of_edges);
	fclose(fin);
}


/* function to read the communities from a .clu file */
void read_communities(char *fileName_clu, int number_of_vertices, int number_of_communities, community_t *communities, int *nodes2community)
{
	int i, comm_id, node_index, temp_index;
	double flow;
	
	FILE *fin = fopen(fileName_clu, "r");
	/* skip the first two lines of the file */
	fscanf(fin, "%*[^\n]\n", NULL);
	fscanf(fin, "%*[^\n]\n", NULL);
	
	for (i = 0; i < number_of_vertices; i++) {
		fscanf(fin, "%d%d%lf", &node_index, &comm_id, &flow);
		comm_id--;
		assert(comm_id < number_of_communities);
		nodes2community[node_index] = comm_id;
		temp_index = communities[comm_id].size++;
		assert(temp_index < MAX_SIZE);
		communities[comm_id].vertices[temp_index] = node_index;
	}
	
	fclose(fin);
}


/* function to print the similarity matrix */
void print_similarity(int number_of_vertices, double *S)
{
	int i, j;
	printf("The following is the similarity matrix based on shortest paths:\n");
	for (i = 0; i < number_of_vertices; i++) {
		for (j = 0; j < number_of_vertices; j++)
			printf("%10lf ", S[i * number_of_vertices + j]);
		printf("\n");
	}
	printf("\n");
}


/* function to print the exemplars */
void print_exemplars(int number_of_exemplars, int *exemplars, node_t *nodes)
{
	int i;
	printf("The number of exemplars obtained = %d\n", number_of_exemplars);
	printf("The following nodes are the exemplars:");
	for (i = 0; i < number_of_exemplars; i++)
		printf("%10d ", nodes[exemplars[i]].ID);
	printf("\n");
}


/* function to print the assignments */
void print_assignments(int number_of_vertices, int *assignments)
{
	int i;
	printf("The following are the assignments of nodes to exemplars:\n");
	for (i = 0; i < number_of_vertices; i++)
		printf("%10d -> %10d\n", i, assignments[i]);
}


/* function to print the total spread of each exemplar */
void print_spread(int number_of_vertices, int *spread)
{
	int i;
	printf("The following is the spread of each exemplar\n");
	spread_t *total_spread = (spread_t *) malloc(sizeof(spread_t) * number_of_vertices);
	for (i = 0; i < number_of_vertices; i++) {
		total_spread[i].index = i;
		total_spread[i].spread = spread[i];
	}
	qsort(total_spread, number_of_vertices, sizeof(spread_t), compare_by_spread);
	for (i = 0; i < number_of_vertices; i++) {
		if (total_spread[i].spread >= 10)
			printf("%10d -> %10d\n", total_spread[i].index, total_spread[i].spread);
	}
	printf("\n");
}



int main(int argc, char *argv[])
{
	int i;												/* counters */
	double time = 0;									/* time counter */
	int number_of_vertices, number_of_edges;			/* number of nodes and edges in the graph */
	int number_of_communities;							/* number of communities in the graph */
	int number_of_iterations;							/* number of iterations affinity propagation runs */
	double lambda = 0.8;								/* damping factor */
	double deadline = (double) INFINITY;				/* deadline */
	clock_t start, stop;								/* variables for measuring time taken by the CPU */
	
	char *fileName_graph = argv[1];						/* file to read graph from passed as an argument in console */
	char *fileName_map = argv[2];						/* file to read number of communities from passed as an argument in console */
	char *fileName_clu = argv[3];						/* file to read the nodes to communities map from passed as an argument in console */
	number_of_iterations = atoi(argv[4]);				/* no. of iterations passed as argument in console */

	printf("Reading communities from file\n");
	start = clock();
	read_number_of_communities(fileName_map, &number_of_vertices, &number_of_edges, &number_of_communities);
	printf("Number of vertices = %d\n", number_of_vertices);
	printf("Number of edges = %d\n", number_of_edges);
	printf("Number of communities = %d\n", number_of_communities);
	int *nodes2community = (int *) malloc(sizeof(int) * number_of_vertices);
	community_t *communities = (community_t *) malloc(sizeof(community_t) * number_of_communities);
	read_communities(fileName_clu, number_of_vertices, number_of_communities, communities, nodes2community);
	stop = clock();
	printf("Done reading communities from file in %lf seconds\n\n", (double) (stop - start)/CLOCKS_PER_SEC);
	time += (double) (stop - start)/CLOCKS_PER_SEC;
	
	int *spread = (int *) malloc(sizeof(int) * number_of_vertices);
	for (i = 0; i < number_of_vertices; i++)
		spread[i] = 0;
	
	printf("Running affinity propagation on all %d communities\n", number_of_communities);
	start = clock();
	for (i = 0; i < number_of_communities; i++) {
		if (communities[i].size > 10) {
			int number_of_induced_vertices = communities[i].size, number_of_induced_edges = 0;
			node_t *induced_nodes = (node_t *) malloc(sizeof(node_t) * number_of_induced_vertices);
			double *induced_weights = (double *) malloc(sizeof(double) * number_of_induced_vertices * MAX_OUTDEGREE);
			
			compute_induced_graph(fileName_graph, i, number_of_vertices, number_of_induced_vertices, &number_of_induced_edges, communities, nodes2community, induced_nodes, induced_weights);

			if (number_of_induced_edges == 0)
				continue;
				
			printf("Starting Community %d\n", i + 1);
			printf("Number of induced vertices for Community %d = %d\n", i + 1, number_of_induced_vertices);
			printf("Number of induced edges for Community %d = %d\n", i + 1, number_of_induced_edges);
			
			if (communities[i].size < MAX_VERTICES)
				community_spread_GPU(number_of_iterations, number_of_induced_vertices, number_of_induced_edges, lambda, deadline, spread, induced_nodes, induced_weights);
			else
				community_spread_CPU(number_of_iterations, number_of_induced_vertices, number_of_induced_edges, lambda, deadline, spread, induced_nodes, induced_weights);
			
			printf("Done Community %d\n", i + 1);
			
			/* free allocated space */
			free(induced_nodes);
			free(induced_weights);
		}
	}
	stop = clock();
	printf("Done running affinity propagation on all communities in %lf seconds\n\n", (double) (stop - start)/CLOCKS_PER_SEC);
	time += (double) (stop - start)/CLOCKS_PER_SEC;

	printf("Final spread of all the exemplars\n");
	print_spread(number_of_vertices, spread);
	printf("\nTotal time taken = %lf seconds.\n", time);
	
	free(spread);
	free(communities);
	
	return 0;
}