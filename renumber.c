/****************************************
	Authors: Koushik Pal
			 Zissis Poulos
****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <time.h>

#define MAX_NODE_NUM 8000

int main(int argc, char *argv[])
{
	int i, u, v, max_k, k, j;					/* counters */
//	double w;
	int node[MAX_NODE_NUM]={0};
	char *fileNameIn = argv[1];		/* input file path passed as an argument in console */
	char *fileNameOut = argv[2];	/* output file path passed as an argument in console */

	FILE *fin = fopen(fileNameIn, "r");
	FILE *fout = fopen(fileNameOut, "w+");
	int number_of_edges;
	
	/* first row of the file is number of edges */
 	fscanf(fin, "%d", &number_of_edges);
	max_k = -1;
	for (i = 0; i < number_of_edges; ++i) {
		k=1;
		j=1;
					
		fscanf(fin, "%d%d", &u, &v);
		while (u != node[j] && j<=max_k){
			++j;	
		}
		node[j] = u;
		if (j>max_k)
			max_k=j;
			
		while (v != node[k] && k<=max_k ){
			++k;	
		}	
		node[k] = v;
		if (k>max_k){
			max_k=k;
		}
		fprintf(fout, "%d %d\n", j, k);
	}
	
	fclose(fin);
	fclose(fout);
	
	return 0;
}
